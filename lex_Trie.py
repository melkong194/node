import json
import _pickle as pkl
import re
import numpy as np
with open("phone_lex.json", mode='r', encoding='utf-8') as jfile:
    LEXICON = json.load(jfile)
LEXICON.sort()

def main():
    my_lex = MyTrie()
class MyTrie:
    def __init__(self):
        self.root = TrieNode()
        self._load_trie_recursively(LEXICON)
    def _load_trie_iteratively(self, lexicon):
        for entry in lexicon:
            current = self.root
            for i, character in enumerate(entry):

                if character in current.children.keys():
                    current = current.children[character]
                else:
                    current.children[character] = TrieNode()
                    current = current.children[character]
                    if i == len(entry):
                        current.is_word = True
    def _load_trie_recursively(self, lexicon):
        current = self.root
        for entry in lexicon:

            self._recurse_load(current, entry)
    def _recurse_load(self, current, seq):

        character = seq[0]
        if character in current.children.keys():
            self._recurse_load(current.children[character], seq[1:])
        else:
            current.children[character] = TrieNode()
            if len(seq) == 1:
 
                current.children[seq].is_word = True
                return
            self._recurse_load(current.children[character], seq[1:])


   
    def valid_words(self, word, verbose):
        current = self.root
        try:
            size = len(word[0])-1
        except: 
            return [[],[]]
        out = []
        final = []
        codes = set()
        for cpt, cc in enumerate(word):
            if cpt==0:
                chars = [word[cpt][x][0] in current.children.keys() or word[cpt][x][0]=="<blk>"  for x in range(len(word[cpt]))]

                for i, elt in enumerate(chars):
                    code = "{}_".format(str(cpt)+str(i))
                    if elt==True:
                        if word[cpt][i][0]=="<blk>":
                            pass
                        else:
                            out.append({"code" : code ,"token" : word[cpt][i][0], "state" : current.children[word[cpt][i][0]], "value" : [float(word[cpt][i][1])]})
                            codes.add(code)
            else:
                new_out = []
                for cand in out:
                    nb_chars = len(cand['value'])
                    l_word = len(cand['token'])
                    if nb_chars/2<=l_word:
                        chars = [word[cpt][x][0] in cand["state"].children.keys() or word[cpt][x][0]=="<blk>"  for x in range(len(word[cpt]))]
                        for i, elt in enumerate(chars):
                            code = "{}_".format(str(cpt)+str(i))
                            code = cand["code"]+code
                            if elt == True:
                                if word[cpt][i][0]=="<blk>":

                                    new_out.append({"code" : cand["code"] ,"token" : cand["token"], "state" : cand["state"], "value" : cand["value"]+[float(word[cpt][i][1])]})
                                elif code not in codes:
                                    new_out.append({"code" : code, "token" : cand["token"]+word[cpt][i][0], "state" : cand["state"].children[word[cpt][i][0]], "value" : cand["value"]+[float(word[cpt][i][1])]})
                                    codes.add(code)
                                else:
                                    recap_codes = [x["code"] for x in out]
                                    recap_value = [np.mean(x["value"]) for x in out]
                                    pos = recap_codes.index(code)
                                    if np.mean(out[pos]["value"])<np.mean(cand["value"]+[float(word[cpt][i][1])]):
                                        new_out.append({"code" : code, "token" : cand["token"]+word[cpt][i][0], "state" : cand["state"].children[word[cpt][i][0]], "value" : cand["value"]+[float(word[cpt][i][1])]})


 
                chars = [word[cpt][x][0] in current.children.keys() or word[cpt][x][0]=="<blk>"  for x in range(len(word[cpt]))]
                for i, elt in enumerate(chars):
                    code = "{}_".format(str(cpt)+str(i))
                    if elt==True:
                        if word[cpt][i][0]=="<blk>":
                            pass
                        else:
                            new_out.append({"code" : code, "token" : word[cpt][i][0], "state" : current.children[word[cpt][i][0]], "value" : [float(word[cpt][i][1])]})
                            codes.add(code)
                out = new_out

            for entry in out:
                if entry["state"].is_word:
                    final.append((entry['token'], np.mean(entry["value"]), entry["code"]))
            recap_codes = [x["code"] for x in out]
            recap_value = [np.mean(x["value"]) for x in out]
            log_mot = [(x["token"], np.mean(x["value"])) for x in out]

        return final, out
class TrieNode:
    def __init__(self):
        self.children = dict()
        self.is_word = False
