module.exports = [
    {
        id: 0,
        name: "MIXPRE-173-20200415-EK-list-orig_0",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_0.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "rre", sound: "/assets/sylls/rre.wav" },
            { id: 1, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 2, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 3, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 4, name: "rne", sound: "/assets/sylls/rne.wav" },
            { id: 5, name: "bu", sound: "/assets/sylls/bu.wav" },
            { id: 6, name: "ba", sound: "/assets/sylls/ba.wav" },
            { id: 7, name: "bun", sound: "/assets/sylls/bun.wav" }
        ]
    },
    {
        id: 1,
        name: "MIXPRE-173-20200415-EK-list-orig_1",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_1.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 1, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 2, name: "na", sound: "/assets/sylls/na.wav" },
            { id: 3, name: "wa", sound: "/assets/sylls/wa.wav" },
            { id: 4, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 5, name: "rre", sound: "/assets/sylls/rre.wav" },
            { id: 6, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 7, name: "bu", sound: "/assets/sylls/bu.wav" },
            { id: 8, name: "bun", sound: "/assets/sylls/bun.wav" }
        ]
    },
    {
        id: 2,
        name: "MIXPRE-173-20200415-EK-list-orig_10",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_10.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 1, name: "wa", sound: "/assets/sylls/wa.wav" },
            { id: 2, name: "ma", sound: "/assets/sylls/ma.wav" },
            { id: 3, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 4, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 5, name: "ngu", sound: "/assets/sylls/ngu.wav" }
        ]
    },
    {
        id: 3,
        name: "MIXPRE-173-20200415-EK-list-orig_100",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_100.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 1, name: "yi", sound: "/assets/sylls/yi.wav" },
            { id: 2, name: "bu", sound: "/assets/sylls/bu.wav" },
            { id: 3, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 4, name: "be", sound: "/assets/sylls/be.wav" },
            { id: 5, name: "bi", sound: "/assets/sylls/bi.wav" },
            { id: 6, name: "ke", sound: "/assets/sylls/ke.wav" },
            { id: 7, name: "kan", sound: "/assets/sylls/kan.wav" }
        ]
    },
    {
        id: 4,
        name: "MIXPRE-173-20200415-EK-list-orig_101",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_101.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 1, name: "ne", sound: "/assets/sylls/ne.wav" },
            { id: 2, name: "bu", sound: "/assets/sylls/bu.wav" },
            { id: 3, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 4, name: "ke", sound: "/assets/sylls/ke.wav" },
            { id: 5, name: "ba", sound: "/assets/sylls/ba.wav" },
            { id: 6, name: "kan", sound: "/assets/sylls/kan.wav" }
        ]
    },
    {
        id: 5,
        name: "MIXPRE-173-20200415-EK-list-orig_102",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_102.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 1, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 2, name: "bi", sound: "/assets/sylls/bi.wav" },
            { id: 3, name: "be", sound: "/assets/sylls/be.wav" },
            { id: 4, name: "ba", sound: "/assets/sylls/ba.wav" },
            { id: 5, name: "ben", sound: "/assets/sylls/ben.wav" },
            { id: 6, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 7, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 8, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 9, name: "me", sound: "/assets/sylls/me.wav" },
            { id: 10, name: "ma", sound: "/assets/sylls/ma.wav" },
            { id: 11, name: "mi", sound: "/assets/sylls/mi.wav" }
        ]
    },
    {
        id: 6,
        name: "MIXPRE-173-20200415-EK-list-orig_103",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_103.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 1, name: "be", sound: "/assets/sylls/be.wav" },
            { id: 2, name: "bi", sound: "/assets/sylls/bi.wav" },
            { id: 3, name: "bu", sound: "/assets/sylls/bu.wav" },
            { id: 4, name: "ba", sound: "/assets/sylls/ba.wav" },
            { id: 5, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 6, name: "me", sound: "/assets/sylls/me.wav" },
            { id: 7, name: "mi", sound: "/assets/sylls/mi.wav" },
            { id: 8, name: "ma", sound: "/assets/sylls/ma.wav" }
        ]
    },
    {
        id: 7,
        name: "MIXPRE-173-20200415-EK-list-orig_104",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_104.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 1, name: "ke", sound: "/assets/sylls/ke.wav" },
            { id: 2, name: "ku", sound: "/assets/sylls/ku.wav" },
            { id: 3, name: "kan", sound: "/assets/sylls/kan.wav" },
            { id: 4, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 5, name: "rre", sound: "/assets/sylls/rre.wav" },
            { id: 6, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 7, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 8, name: "ma", sound: "/assets/sylls/ma.wav" },
            { id: 9, name: "man", sound: "/assets/sylls/man.wav" },
            { id: 10, name: "marn", sound: "/assets/sylls/marn.wav" },
            { id: 11, name: "bu", sound: "/assets/sylls/bu.wav" },
            { id: 12, name: "bun", sound: "/assets/sylls/bun.wav" },
            { id: 13, name: "rren", sound: "/assets/sylls/rren.wav" }
        ]
    },
    {
        id: 8,
        name: "MIXPRE-173-20200415-EK-list-orig_105",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_105.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 1, name: "bi", sound: "/assets/sylls/bi.wav" },
            { id: 2, name: "wa", sound: "/assets/sylls/wa.wav" },
            { id: 3, name: "yi", sound: "/assets/sylls/yi.wav" },
            { id: 4, name: "bu", sound: "/assets/sylls/bu.wav" },
            { id: 5, name: "me", sound: "/assets/sylls/me.wav" },
            { id: 6, name: "ma", sound: "/assets/sylls/ma.wav" },
            { id: 7, name: "ngu", sound: "/assets/sylls/ngu.wav" },
            { id: 8, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 9, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 10, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 11, name: "ngan", sound: "/assets/sylls/ngan.wav" },
            { id: 12, name: "man", sound: "/assets/sylls/man.wav" },
            { id: 13, name: "rre", sound: "/assets/sylls/rre.wav" },
            { id: 14, name: "di", sound: "/assets/sylls/di.wav" }
        ]
    },
    {
        id: 9,
        name: "MIXPRE-173-20200415-EK-list-orig_106",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_106.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 1, name: "bi", sound: "/assets/sylls/bi.wav" },
            { id: 2, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 3, name: "rre", sound: "/assets/sylls/rre.wav" },
            { id: 4, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 5, name: "ben", sound: "/assets/sylls/ben.wav" },
            { id: 6, name: "rren", sound: "/assets/sylls/rren.wav" },
            { id: 7, name: "na", sound: "/assets/sylls/na.wav" },
            { id: 8, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 9, name: "ngan", sound: "/assets/sylls/ngan.wav" },
            { id: 10, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 11, name: "ne", sound: "/assets/sylls/ne.wav" },
            { id: 12, name: "rne", sound: "/assets/sylls/rne.wav" }
        ]
    },
    {
        id: 10,
        name: "MIXPRE-173-20200415-EK-list-orig_107",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_107.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ke", sound: "/assets/sylls/ke.wav" },
            { id: 1, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 2, name: "ku", sound: "/assets/sylls/ku.wav" },
            { id: 3, name: "bi", sound: "/assets/sylls/bi.wav" },
            { id: 4, name: "kan", sound: "/assets/sylls/kan.wav" },
            { id: 5, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 6, name: "rre", sound: "/assets/sylls/rre.wav" },
            { id: 7, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 8, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 9, name: "rren", sound: "/assets/sylls/rren.wav" },
            { id: 10, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 11, name: "na", sound: "/assets/sylls/na.wav" },
            { id: 12, name: "ngan", sound: "/assets/sylls/ngan.wav" },
            { id: 13, name: "ne", sound: "/assets/sylls/ne.wav" },
            { id: 14, name: "me", sound: "/assets/sylls/me.wav" },
            { id: 15, name: "rne", sound: "/assets/sylls/rne.wav" }
        ]
    },
    {
        id: 11,
        name: "MIXPRE-173-20200415-EK-list-orig_108",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_108.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "bi", sound: "/assets/sylls/bi.wav" },
            { id: 1, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 2, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 3, name: "rre", sound: "/assets/sylls/rre.wav" },
            { id: 4, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 5, name: "rren", sound: "/assets/sylls/rren.wav" },
            { id: 6, name: "na", sound: "/assets/sylls/na.wav" },
            { id: 7, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 8, name: "ngan", sound: "/assets/sylls/ngan.wav" },
            { id: 9, name: "ne", sound: "/assets/sylls/ne.wav" },
            { id: 10, name: "mi", sound: "/assets/sylls/mi.wav" },
            { id: 11, name: "me", sound: "/assets/sylls/me.wav" }
        ]
    },
    {
        id: 12,
        name: "MIXPRE-173-20200415-EK-list-orig_109",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_109.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "bi", sound: "/assets/sylls/bi.wav" },
            { id: 1, name: "be", sound: "/assets/sylls/be.wav" },
            { id: 2, name: "ba", sound: "/assets/sylls/ba.wav" },
            { id: 3, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 4, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 5, name: "rre", sound: "/assets/sylls/rre.wav" },
            { id: 6, name: "rren", sound: "/assets/sylls/rren.wav" },
            { id: 7, name: "na", sound: "/assets/sylls/na.wav" },
            { id: 8, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 9, name: "ngan", sound: "/assets/sylls/ngan.wav" },
            { id: 10, name: "ni", sound: "/assets/sylls/ni.wav" }
        ]
    },
    {
        id: 13,
        name: "MIXPRE-173-20200415-EK-list-orig_11",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_11.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 1, name: "rre", sound: "/assets/sylls/rre.wav" },
            { id: 2, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 3, name: "yi", sound: "/assets/sylls/yi.wav" },
            { id: 4, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 5, name: "ke", sound: "/assets/sylls/ke.wav" },
            { id: 6, name: "ku", sound: "/assets/sylls/ku.wav" },
            { id: 7, name: "kan", sound: "/assets/sylls/kan.wav" },
            { id: 8, name: "le", sound: "/assets/sylls/le.wav" },
            { id: 9, name: "rne", sound: "/assets/sylls/rne.wav" },
            { id: 10, name: "me", sound: "/assets/sylls/me.wav" },
            { id: 11, name: "mi", sound: "/assets/sylls/mi.wav" },
            { id: 12, name: "ma", sound: "/assets/sylls/ma.wav" },
            { id: 13, name: "meng", sound: "/assets/sylls/meng.wav" },
            { id: 14, name: "men", sound: "/assets/sylls/men.wav" }
        ]
    },
    {
        id: 14,
        name: "MIXPRE-173-20200415-EK-list-orig_110",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_110.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 1, name: "bi", sound: "/assets/sylls/bi.wav" },
            { id: 2, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 3, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 4, name: "rre", sound: "/assets/sylls/rre.wav" },
            { id: 5, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 6, name: "rren", sound: "/assets/sylls/rren.wav" },
            { id: 7, name: "na", sound: "/assets/sylls/na.wav" },
            { id: 8, name: "nga", sound: "/assets/sylls/nga.wav" }
        ]
    },
    {
        id: 15,
        name: "MIXPRE-173-20200415-EK-list-orig_111",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_111.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "be", sound: "/assets/sylls/be.wav" },
            { id: 1, name: "ba", sound: "/assets/sylls/ba.wav" },
            { id: 2, name: "bi", sound: "/assets/sylls/bi.wav" },
            { id: 3, name: "ben", sound: "/assets/sylls/ben.wav" },
            { id: 4, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 5, name: "rre", sound: "/assets/sylls/rre.wav" },
            { id: 6, name: "yi", sound: "/assets/sylls/yi.wav" },
            { id: 7, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 8, name: "wa", sound: "/assets/sylls/wa.wav" },
            { id: 9, name: "ku", sound: "/assets/sylls/ku.wav" },
            { id: 10, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 11, name: "bu", sound: "/assets/sylls/bu.wav" },
            { id: 12, name: "me", sound: "/assets/sylls/me.wav" },
            { id: 13, name: "mi", sound: "/assets/sylls/mi.wav" },
            { id: 14, name: "ma", sound: "/assets/sylls/ma.wav" }
        ]
    },
    {
        id: 16,
        name: "MIXPRE-173-20200415-EK-list-orig_112",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_112.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 1, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 2, name: "na", sound: "/assets/sylls/na.wav" },
            { id: 3, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 4, name: "rre", sound: "/assets/sylls/rre.wav" },
            { id: 5, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 6, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 7, name: "ku", sound: "/assets/sylls/ku.wav" },
            { id: 8, name: "bu", sound: "/assets/sylls/bu.wav" },
            { id: 9, name: "nge", sound: "/assets/sylls/nge.wav" },
            { id: 10, name: "ngu", sound: "/assets/sylls/ngu.wav" },
            { id: 11, name: "me", sound: "/assets/sylls/me.wav" }
        ]
    },
    {
        id: 17,
        name: "MIXPRE-173-20200415-EK-list-orig_113",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_113.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 1, name: "bi", sound: "/assets/sylls/bi.wav" },
            { id: 2, name: "be", sound: "/assets/sylls/be.wav" },
            { id: 3, name: "ba", sound: "/assets/sylls/ba.wav" },
            { id: 4, name: "bu", sound: "/assets/sylls/bu.wav" },
            { id: 5, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 6, name: "ben", sound: "/assets/sylls/ben.wav" },
            { id: 7, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 8, name: "rre", sound: "/assets/sylls/rre.wav" },
            { id: 9, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 10, name: "ku", sound: "/assets/sylls/ku.wav" },
            { id: 11, name: "kun", sound: "/assets/sylls/kun.wav" },
            { id: 12, name: "kan", sound: "/assets/sylls/kan.wav" }
        ]
    },
    {
        id: 18,
        name: "MIXPRE-173-20200415-EK-list-orig_114",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_114.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 1, name: "ma", sound: "/assets/sylls/ma.wav" },
            { id: 2, name: "me", sound: "/assets/sylls/me.wav" },
            { id: 3, name: "wa", sound: "/assets/sylls/wa.wav" },
            { id: 4, name: "na", sound: "/assets/sylls/na.wav" },
            { id: 5, name: "ngan", sound: "/assets/sylls/ngan.wav" },
            { id: 6, name: "rre", sound: "/assets/sylls/rre.wav" },
            { id: 7, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 8, name: "ne", sound: "/assets/sylls/ne.wav" },
            { id: 9, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 10, name: "rne", sound: "/assets/sylls/rne.wav" },
            { id: 11, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 12, name: "karr", sound: "/assets/sylls/karr.wav" },
            { id: 13, name: "kan", sound: "/assets/sylls/kan.wav" },
            { id: 14, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 15, name: "bu", sound: "/assets/sylls/bu.wav" },
            { id: 16, name: "bi", sound: "/assets/sylls/bi.wav" },
            { id: 17, name: "ngu", sound: "/assets/sylls/ngu.wav" }
        ]
    },
    {
        id: 19,
        name: "MIXPRE-173-20200415-EK-list-orig_115",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_115.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 1, name: "rre", sound: "/assets/sylls/rre.wav" },
            { id: 2, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 3, name: "ya", sound: "/assets/sylls/ya.wav" },
            { id: 4, name: "kan", sound: "/assets/sylls/kan.wav" },
            { id: 5, name: "djal", sound: "/assets/sylls/djal.wav" },
            { id: 6, name: "me", sound: "/assets/sylls/me.wav" },
            { id: 7, name: "mi", sound: "/assets/sylls/mi.wav" },
            { id: 8, name: "ma", sound: "/assets/sylls/ma.wav" }
        ]
    },
    {
        id: 20,
        name: "MIXPRE-173-20200415-EK-list-orig_116",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_116.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 1, name: "na", sound: "/assets/sylls/na.wav" },
            { id: 2, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 3, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 4, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 5, name: "karr", sound: "/assets/sylls/karr.wav" },
            { id: 6, name: "djal", sound: "/assets/sylls/djal.wav" },
            { id: 7, name: "kan", sound: "/assets/sylls/kan.wav" },
            { id: 8, name: "me", sound: "/assets/sylls/me.wav" },
            { id: 9, name: "mi", sound: "/assets/sylls/mi.wav" },
            { id: 10, name: "ma", sound: "/assets/sylls/ma.wav" }
        ]
    },
    {
        id: 21,
        name: "MIXPRE-173-20200415-EK-list-orig_117",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_117.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 1, name: "bi", sound: "/assets/sylls/bi.wav" },
            { id: 2, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 3, name: "me", sound: "/assets/sylls/me.wav" },
            { id: 4, name: "ma", sound: "/assets/sylls/ma.wav" },
            { id: 5, name: "mi", sound: "/assets/sylls/mi.wav" }
        ]
    },
    {
        id: 22,
        name: "MIXPRE-173-20200415-EK-list-orig_118",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_118.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 1, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 2, name: "be", sound: "/assets/sylls/be.wav" },
            { id: 3, name: "bi", sound: "/assets/sylls/bi.wav" },
            { id: 4, name: "bu", sound: "/assets/sylls/bu.wav" },
            { id: 5, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 6, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 7, name: "ben", sound: "/assets/sylls/ben.wav" },
            { id: 8, name: "me", sound: "/assets/sylls/me.wav" },
            { id: 9, name: "mi", sound: "/assets/sylls/mi.wav" },
            { id: 10, name: "ma", sound: "/assets/sylls/ma.wav" }
        ]
    },
    {
        id: 23,
        name: "MIXPRE-173-20200415-EK-list-orig_119",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_119.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 1, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 2, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 3, name: "rre", sound: "/assets/sylls/rre.wav" },
            { id: 4, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 5, name: "wa", sound: "/assets/sylls/wa.wav" },
            { id: 6, name: "ma", sound: "/assets/sylls/ma.wav" },
            { id: 7, name: "me", sound: "/assets/sylls/me.wav" }
        ]
    },
    {
        id: 24,
        name: "MIXPRE-173-20200415-EK-list-orig_12",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_12.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 1, name: "ke", sound: "/assets/sylls/ke.wav" },
            { id: 2, name: "bi", sound: "/assets/sylls/bi.wav" },
            { id: 3, name: "wa", sound: "/assets/sylls/wa.wav" },
            { id: 4, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 5, name: "kan", sound: "/assets/sylls/kan.wav" },
            { id: 6, name: "rre", sound: "/assets/sylls/rre.wav" },
            { id: 7, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 8, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 9, name: "rne", sound: "/assets/sylls/rne.wav" },
            { id: 10, name: "ya", sound: "/assets/sylls/ya.wav" },
            { id: 11, name: "me", sound: "/assets/sylls/me.wav" },
            { id: 12, name: "mi", sound: "/assets/sylls/mi.wav" },
            { id: 13, name: "ne", sound: "/assets/sylls/ne.wav" }
        ]
    },
    {
        id: 25,
        name: "MIXPRE-173-20200415-EK-list-orig_120",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_120.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 1, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 2, name: "na", sound: "/assets/sylls/na.wav" },
            { id: 3, name: "bi", sound: "/assets/sylls/bi.wav" },
            { id: 4, name: "be", sound: "/assets/sylls/be.wav" },
            { id: 5, name: "ba", sound: "/assets/sylls/ba.wav" },
            { id: 6, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 7, name: "rre", sound: "/assets/sylls/rre.wav" },
            { id: 8, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 9, name: "rren", sound: "/assets/sylls/rren.wav" },
            { id: 10, name: "wa", sound: "/assets/sylls/wa.wav" },
            { id: 11, name: "ma", sound: "/assets/sylls/ma.wav" },
            { id: 12, name: "me", sound: "/assets/sylls/me.wav" },
            { id: 13, name: "ni", sound: "/assets/sylls/ni.wav" }
        ]
    },
    {
        id: 26,
        name: "MIXPRE-173-20200415-EK-list-orig_121",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_121.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 1, name: "bi", sound: "/assets/sylls/bi.wav" },
            { id: 2, name: "bu", sound: "/assets/sylls/bu.wav" },
            { id: 3, name: "be", sound: "/assets/sylls/be.wav" },
            { id: 4, name: "ba", sound: "/assets/sylls/ba.wav" },
            { id: 5, name: "ben", sound: "/assets/sylls/ben.wav" },
            { id: 6, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 7, name: "rre", sound: "/assets/sylls/rre.wav" },
            { id: 8, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 9, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 10, name: "ke", sound: "/assets/sylls/ke.wav" },
            { id: 11, name: "kan", sound: "/assets/sylls/kan.wav" }
        ]
    },
    {
        id: 27,
        name: "MIXPRE-173-20200415-EK-list-orig_122",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_122.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 1, name: "ku", sound: "/assets/sylls/ku.wav" },
            { id: 2, name: "ba", sound: "/assets/sylls/ba.wav" },
            { id: 3, name: "be", sound: "/assets/sylls/be.wav" },
            { id: 4, name: "bi", sound: "/assets/sylls/bi.wav" },
            { id: 5, name: "ma", sound: "/assets/sylls/ma.wav" },
            { id: 6, name: "me", sound: "/assets/sylls/me.wav" },
            { id: 7, name: "wa", sound: "/assets/sylls/wa.wav" },
            { id: 8, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 9, name: "ke", sound: "/assets/sylls/ke.wav" },
            { id: 10, name: "bu", sound: "/assets/sylls/bu.wav" },
            { id: 11, name: "kuk", sound: "/assets/sylls/kuk.wav" },
            { id: 12, name: "kka", sound: "/assets/sylls/kka.wav" }
        ]
    },
    {
        id: 28,
        name: "MIXPRE-173-20200415-EK-list-orig_123",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_123.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "bi", sound: "/assets/sylls/bi.wav" },
            { id: 1, name: "be", sound: "/assets/sylls/be.wav" },
            { id: 2, name: "bu", sound: "/assets/sylls/bu.wav" },
            { id: 3, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 4, name: "ben", sound: "/assets/sylls/ben.wav" },
            { id: 5, name: "kan", sound: "/assets/sylls/kan.wav" },
            { id: 6, name: "ne", sound: "/assets/sylls/ne.wav" },
            { id: 7, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 8, name: "rre", sound: "/assets/sylls/rre.wav" },
            { id: 9, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 10, name: "rne", sound: "/assets/sylls/rne.wav" },
            { id: 11, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 12, name: "bun", sound: "/assets/sylls/bun.wav" },
            { id: 13, name: "na", sound: "/assets/sylls/na.wav" }
        ]
    },
    {
        id: 29,
        name: "MIXPRE-173-20200415-EK-list-orig_124",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_124.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 1, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 2, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 3, name: "be", sound: "/assets/sylls/be.wav" },
            { id: 4, name: "bi", sound: "/assets/sylls/bi.wav" },
            { id: 5, name: "ke", sound: "/assets/sylls/ke.wav" },
            { id: 6, name: "ben", sound: "/assets/sylls/ben.wav" },
            { id: 7, name: "bu", sound: "/assets/sylls/bu.wav" },
            { id: 8, name: "bo", sound: "/assets/sylls/bo.wav" },
            { id: 9, name: "ba", sound: "/assets/sylls/ba.wav" }
        ]
    },
    {
        id: 30,
        name: "MIXPRE-173-20200415-EK-list-orig_125",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_125.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ke", sound: "/assets/sylls/ke.wav" },
            { id: 1, name: "bi", sound: "/assets/sylls/bi.wav" },
            { id: 2, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 3, name: "rre", sound: "/assets/sylls/rre.wav" },
            { id: 4, name: "bu", sound: "/assets/sylls/bu.wav" },
            { id: 5, name: "be", sound: "/assets/sylls/be.wav" },
            { id: 6, name: "ba", sound: "/assets/sylls/ba.wav" },
            { id: 7, name: "beb", sound: "/assets/sylls/beb.wav" },
            { id: 8, name: "ku", sound: "/assets/sylls/ku.wav" },
            { id: 9, name: "bun", sound: "/assets/sylls/bun.wav" },
            { id: 10, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 11, name: "mi", sound: "/assets/sylls/mi.wav" }
        ]
    },
    {
        id: 31,
        name: "MIXPRE-173-20200415-EK-list-orig_126",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_126.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 1, name: "kan", sound: "/assets/sylls/kan.wav" },
            { id: 2, name: "ngu", sound: "/assets/sylls/ngu.wav" }
        ]
    },
    {
        id: 32,
        name: "MIXPRE-173-20200415-EK-list-orig_127",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_127.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 1, name: "kan", sound: "/assets/sylls/kan.wav" },
            { id: 2, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 3, name: "ngu", sound: "/assets/sylls/ngu.wav" }
        ]
    },
    {
        id: 33,
        name: "MIXPRE-173-20200415-EK-list-orig_128",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_128.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 1, name: "bi", sound: "/assets/sylls/bi.wav" },
            { id: 2, name: "be", sound: "/assets/sylls/be.wav" },
            { id: 3, name: "mi", sound: "/assets/sylls/mi.wav" },
            { id: 4, name: "me", sound: "/assets/sylls/me.wav" },
            { id: 5, name: "ma", sound: "/assets/sylls/ma.wav" },
            { id: 6, name: "ben", sound: "/assets/sylls/ben.wav" },
            { id: 7, name: "men", sound: "/assets/sylls/men.wav" },
            { id: 8, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 9, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 10, name: "bu", sound: "/assets/sylls/bu.wav" },
            { id: 11, name: "ba", sound: "/assets/sylls/ba.wav" },
            { id: 12, name: "ku", sound: "/assets/sylls/ku.wav" },
            { id: 13, name: "kuk", sound: "/assets/sylls/kuk.wav" },
            { id: 14, name: "bun", sound: "/assets/sylls/bun.wav" },
            { id: 15, name: "kan", sound: "/assets/sylls/kan.wav" }
        ]
    },
    {
        id: 34,
        name: "MIXPRE-173-20200415-EK-list-orig_129",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_129.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 1, name: "ke", sound: "/assets/sylls/ke.wav" },
            { id: 2, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 3, name: "kan", sound: "/assets/sylls/kan.wav" },
            { id: 4, name: "me", sound: "/assets/sylls/me.wav" },
            { id: 5, name: "mi", sound: "/assets/sylls/mi.wav" },
            { id: 6, name: "ma", sound: "/assets/sylls/ma.wav" },
            { id: 7, name: "ne", sound: "/assets/sylls/ne.wav" },
            { id: 8, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 9, name: "be", sound: "/assets/sylls/be.wav" },
            { id: 10, name: "bi", sound: "/assets/sylls/bi.wav" },
            { id: 11, name: "rne", sound: "/assets/sylls/rne.wav" },
            { id: 12, name: "men", sound: "/assets/sylls/men.wav" },
            { id: 13, name: "ben", sound: "/assets/sylls/ben.wav" },
            { id: 14, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 15, name: "bu", sound: "/assets/sylls/bu.wav" },
            { id: 16, name: "ku", sound: "/assets/sylls/ku.wav" },
            { id: 17, name: "kuk", sound: "/assets/sylls/kuk.wav" },
            { id: 18, name: "ba", sound: "/assets/sylls/ba.wav" },
            { id: 19, name: "bun", sound: "/assets/sylls/bun.wav" }
        ]
    },
    {
        id: 35,
        name: "MIXPRE-173-20200415-EK-list-orig_13",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_13.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 1, name: "ya", sound: "/assets/sylls/ya.wav" },
            { id: 2, name: "djal", sound: "/assets/sylls/djal.wav" },
            { id: 3, name: "rre", sound: "/assets/sylls/rre.wav" },
            { id: 4, name: "ne", sound: "/assets/sylls/ne.wav" },
            { id: 5, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 6, name: "me", sound: "/assets/sylls/me.wav" },
            { id: 7, name: "yi", sound: "/assets/sylls/yi.wav" }
        ]
    },
    {
        id: 36,
        name: "MIXPRE-173-20200415-EK-list-orig_130",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_130.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 1, name: "wa", sound: "/assets/sylls/wa.wav" },
            { id: 2, name: "bi", sound: "/assets/sylls/bi.wav" },
            { id: 3, name: "be", sound: "/assets/sylls/be.wav" },
            { id: 4, name: "ba", sound: "/assets/sylls/ba.wav" },
            { id: 5, name: "mi", sound: "/assets/sylls/mi.wav" },
            { id: 6, name: "bin", sound: "/assets/sylls/bin.wav" },
            { id: 7, name: "ben", sound: "/assets/sylls/ben.wav" },
            { id: 8, name: "men", sound: "/assets/sylls/men.wav" },
            { id: 9, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 10, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 11, name: "bu", sound: "/assets/sylls/bu.wav" },
            { id: 12, name: "ke", sound: "/assets/sylls/ke.wav" },
            { id: 13, name: "bun", sound: "/assets/sylls/bun.wav" },
            { id: 14, name: "kan", sound: "/assets/sylls/kan.wav" }
        ]
    },
    {
        id: 37,
        name: "MIXPRE-173-20200415-EK-list-orig_131",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_131.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 1, name: "ku", sound: "/assets/sylls/ku.wav" },
            { id: 2, name: "bi", sound: "/assets/sylls/bi.wav" },
            { id: 3, name: "be", sound: "/assets/sylls/be.wav" },
            { id: 4, name: "bu", sound: "/assets/sylls/bu.wav" },
            { id: 5, name: "me", sound: "/assets/sylls/me.wav" },
            { id: 6, name: "kan", sound: "/assets/sylls/kan.wav" },
            { id: 7, name: "ben", sound: "/assets/sylls/ben.wav" },
            { id: 8, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 9, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 10, name: "kuk", sound: "/assets/sylls/kuk.wav" },
            { id: 11, name: "ba", sound: "/assets/sylls/ba.wav" }
        ]
    },
    {
        id: 38,
        name: "MIXPRE-173-20200415-EK-list-orig_132",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_132.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "bu", sound: "/assets/sylls/bu.wav" },
            { id: 1, name: "be", sound: "/assets/sylls/be.wav" },
            { id: 2, name: "ku", sound: "/assets/sylls/ku.wav" },
            { id: 3, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 4, name: "bun", sound: "/assets/sylls/bun.wav" },
            { id: 5, name: "ben", sound: "/assets/sylls/ben.wav" },
            { id: 6, name: "kun", sound: "/assets/sylls/kun.wav" },
            { id: 7, name: "kan", sound: "/assets/sylls/kan.wav" }
        ]
    },
    {
        id: 39,
        name: "MIXPRE-173-20200415-EK-list-orig_133",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_133.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "bu", sound: "/assets/sylls/bu.wav" },
            { id: 1, name: "be", sound: "/assets/sylls/be.wav" },
            { id: 2, name: "ku", sound: "/assets/sylls/ku.wav" },
            { id: 3, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 4, name: "bun", sound: "/assets/sylls/bun.wav" },
            { id: 5, name: "ben", sound: "/assets/sylls/ben.wav" },
            { id: 6, name: "kan", sound: "/assets/sylls/kan.wav" },
            { id: 7, name: "wa", sound: "/assets/sylls/wa.wav" }
        ]
    },
    {
        id: 40,
        name: "MIXPRE-173-20200415-EK-list-orig_134",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_134.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ma", sound: "/assets/sylls/ma.wav" },
            { id: 1, name: "wa", sound: "/assets/sylls/wa.wav" },
            { id: 2, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 3, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 4, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 5, name: "bi", sound: "/assets/sylls/bi.wav" }
        ]
    },
    {
        id: 41,
        name: "MIXPRE-173-20200415-EK-list-orig_135",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_135.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ma", sound: "/assets/sylls/ma.wav" },
            { id: 1, name: "me", sound: "/assets/sylls/me.wav" },
            { id: 2, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 3, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 4, name: "na", sound: "/assets/sylls/na.wav" },
            { id: 5, name: "yi", sound: "/assets/sylls/yi.wav" }
        ]
    },
    {
        id: 42,
        name: "MIXPRE-173-20200415-EK-list-orig_136",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_136.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ku", sound: "/assets/sylls/ku.wav" },
            { id: 1, name: "ke", sound: "/assets/sylls/ke.wav" },
            { id: 2, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 3, name: "kun", sound: "/assets/sylls/kun.wav" },
            { id: 4, name: "kan", sound: "/assets/sylls/kan.wav" },
            { id: 5, name: "ben", sound: "/assets/sylls/ben.wav" },
            { id: 6, name: "na", sound: "/assets/sylls/na.wav" },
            { id: 7, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 8, name: "ya", sound: "/assets/sylls/ya.wav" },
            { id: 9, name: "be", sound: "/assets/sylls/be.wav" },
            { id: 10, name: "bi", sound: "/assets/sylls/bi.wav" },
            { id: 11, name: "ba", sound: "/assets/sylls/ba.wav" },
            { id: 12, name: "bu", sound: "/assets/sylls/bu.wav" },
            { id: 13, name: "me", sound: "/assets/sylls/me.wav" },
            { id: 14, name: "ma", sound: "/assets/sylls/ma.wav" }
        ]
    },
    {
        id: 43,
        name: "MIXPRE-173-20200415-EK-list-orig_137",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_137.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ku", sound: "/assets/sylls/ku.wav" },
            { id: 1, name: "bu", sound: "/assets/sylls/bu.wav" },
            { id: 2, name: "bi", sound: "/assets/sylls/bi.wav" },
            { id: 3, name: "ngu", sound: "/assets/sylls/ngu.wav" },
            { id: 4, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 5, name: "na", sound: "/assets/sylls/na.wav" }
        ]
    },
    {
        id: 44,
        name: "MIXPRE-173-20200415-EK-list-orig_138",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_138.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ku", sound: "/assets/sylls/ku.wav" },
            { id: 1, name: "ke", sound: "/assets/sylls/ke.wav" },
            { id: 2, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 3, name: "bu", sound: "/assets/sylls/bu.wav" },
            { id: 4, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 5, name: "wa", sound: "/assets/sylls/wa.wav" },
            { id: 6, name: "ma", sound: "/assets/sylls/ma.wav" },
            { id: 7, name: "me", sound: "/assets/sylls/me.wav" },
            { id: 8, name: "kuk", sound: "/assets/sylls/kuk.wav" },
            { id: 9, name: "di", sound: "/assets/sylls/di.wav" }
        ]
    },
    {
        id: 45,
        name: "MIXPRE-173-20200415-EK-list-orig_139",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_139.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ku", sound: "/assets/sylls/ku.wav" },
            { id: 1, name: "ke", sound: "/assets/sylls/ke.wav" },
            { id: 2, name: "bu", sound: "/assets/sylls/bu.wav" },
            { id: 3, name: "bi", sound: "/assets/sylls/bi.wav" },
            { id: 4, name: "kun", sound: "/assets/sylls/kun.wav" },
            { id: 5, name: "bun", sound: "/assets/sylls/bun.wav" },
            { id: 6, name: "ben", sound: "/assets/sylls/ben.wav" },
            { id: 7, name: "na", sound: "/assets/sylls/na.wav" },
            { id: 8, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 9, name: "ma", sound: "/assets/sylls/ma.wav" },
            { id: 10, name: "wa", sound: "/assets/sylls/wa.wav" },
            { id: 11, name: "me", sound: "/assets/sylls/me.wav" },
            { id: 12, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 13, name: "ka", sound: "/assets/sylls/ka.wav" }
        ]
    },
    {
        id: 46,
        name: "MIXPRE-173-20200415-EK-list-orig_14",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_14.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ma", sound: "/assets/sylls/ma.wav" },
            { id: 1, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 2, name: "na", sound: "/assets/sylls/na.wav" },
            { id: 3, name: "man", sound: "/assets/sylls/man.wav" },
            { id: 4, name: "marn", sound: "/assets/sylls/marn.wav" },
            { id: 5, name: "ngan", sound: "/assets/sylls/ngan.wav" },
            { id: 6, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 7, name: "karr", sound: "/assets/sylls/karr.wav" },
            { id: 8, name: "rre", sound: "/assets/sylls/rre.wav" },
            { id: 9, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 10, name: "di", sound: "/assets/sylls/di.wav" }
        ]
    },
    {
        id: 47,
        name: "MIXPRE-173-20200415-EK-list-orig_140",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_140.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 1, name: "na", sound: "/assets/sylls/na.wav" },
            { id: 2, name: "ma", sound: "/assets/sylls/ma.wav" },
            { id: 3, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 4, name: "ngan", sound: "/assets/sylls/ngan.wav" },
            { id: 5, name: "kan", sound: "/assets/sylls/kan.wav" },
            { id: 6, name: "ngu", sound: "/assets/sylls/ngu.wav" },
            { id: 7, name: "ku", sound: "/assets/sylls/ku.wav" },
            { id: 8, name: "ke", sound: "/assets/sylls/ke.wav" },
            { id: 9, name: "bu", sound: "/assets/sylls/bu.wav" }
        ]
    },
    {
        id: 48,
        name: "MIXPRE-173-20200415-EK-list-orig_141",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_141.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ma", sound: "/assets/sylls/ma.wav" },
            { id: 1, name: "na", sound: "/assets/sylls/na.wav" },
            { id: 2, name: "ba", sound: "/assets/sylls/ba.wav" },
            { id: 3, name: "mak", sound: "/assets/sylls/mak.wav" },
            { id: 4, name: "ku", sound: "/assets/sylls/ku.wav" },
            { id: 5, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 6, name: "bu", sound: "/assets/sylls/bu.wav" },
            { id: 7, name: "ngu", sound: "/assets/sylls/ngu.wav" }
        ]
    },
    {
        id: 49,
        name: "MIXPRE-173-20200415-EK-list-orig_142",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_142.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 1, name: "ngu", sound: "/assets/sylls/ngu.wav" },
            { id: 2, name: "ma", sound: "/assets/sylls/ma.wav" },
            { id: 3, name: "di", sound: "/assets/sylls/di.wav" }
        ]
    },
    {
        id: 50,
        name: "MIXPRE-173-20200415-EK-list-orig_143",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_143.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ma", sound: "/assets/sylls/ma.wav" },
            { id: 1, name: "me", sound: "/assets/sylls/me.wav" },
            { id: 2, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 3, name: "wa", sound: "/assets/sylls/wa.wav" },
            { id: 4, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 5, name: "na", sound: "/assets/sylls/na.wav" },
            { id: 6, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 7, name: "ni", sound: "/assets/sylls/ni.wav" }
        ]
    },
    {
        id: 51,
        name: "MIXPRE-173-20200415-EK-list-orig_144",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_144.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ma", sound: "/assets/sylls/ma.wav" },
            { id: 1, name: "me", sound: "/assets/sylls/me.wav" },
            { id: 2, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 3, name: "wa", sound: "/assets/sylls/wa.wav" },
            { id: 4, name: "ba", sound: "/assets/sylls/ba.wav" },
            { id: 5, name: "man", sound: "/assets/sylls/man.wav" },
            { id: 6, name: "men", sound: "/assets/sylls/men.wav" },
            { id: 7, name: "ngan", sound: "/assets/sylls/ngan.wav" },
            { id: 8, name: "ben", sound: "/assets/sylls/ben.wav" },
            { id: 9, name: "ku", sound: "/assets/sylls/ku.wav" },
            { id: 10, name: "le", sound: "/assets/sylls/le.wav" },
            { id: 11, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 12, name: "rne", sound: "/assets/sylls/rne.wav" }
        ]
    },
    {
        id: 52,
        name: "MIXPRE-173-20200415-EK-list-orig_145",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_145.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "me", sound: "/assets/sylls/me.wav" },
            { id: 1, name: "mi", sound: "/assets/sylls/mi.wav" },
            { id: 2, name: "ma", sound: "/assets/sylls/ma.wav" },
            { id: 3, name: "bi", sound: "/assets/sylls/bi.wav" },
            { id: 4, name: "bo", sound: "/assets/sylls/bo.wav" },
            { id: 5, name: "bu", sound: "/assets/sylls/bu.wav" },
            { id: 6, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 7, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 8, name: "le", sound: "/assets/sylls/le.wav" },
            { id: 9, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 10, name: "di", sound: "/assets/sylls/di.wav" }
        ]
    },
    {
        id: 53,
        name: "MIXPRE-173-20200415-EK-list-orig_146",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_146.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ne", sound: "/assets/sylls/ne.wav" },
            { id: 1, name: "na", sound: "/assets/sylls/na.wav" },
            { id: 2, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 3, name: "rne", sound: "/assets/sylls/rne.wav" },
            { id: 4, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 5, name: "bi", sound: "/assets/sylls/bi.wav" },
            { id: 6, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 7, name: "ke", sound: "/assets/sylls/ke.wav" },
            { id: 8, name: "ku", sound: "/assets/sylls/ku.wav" }
        ]
    },
    {
        id: 54,
        name: "MIXPRE-173-20200415-EK-list-orig_147",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_147.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 1, name: "be", sound: "/assets/sylls/be.wav" },
            { id: 2, name: "bi", sound: "/assets/sylls/bi.wav" },
            { id: 3, name: "ke", sound: "/assets/sylls/ke.wav" },
            { id: 4, name: "ka", sound: "/assets/sylls/ka.wav" }
        ]
    },
    {
        id: 55,
        name: "MIXPRE-173-20200415-EK-list-orig_148",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_148.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 1, name: "ngu", sound: "/assets/sylls/ngu.wav" },
            { id: 2, name: "wa", sound: "/assets/sylls/wa.wav" },
            { id: 3, name: "ma", sound: "/assets/sylls/ma.wav" },
            { id: 4, name: "ngan", sound: "/assets/sylls/ngan.wav" },
            { id: 5, name: "ngun", sound: "/assets/sylls/ngun.wav" },
            { id: 6, name: "le", sound: "/assets/sylls/le.wav" },
            { id: 7, name: "ne", sound: "/assets/sylls/ne.wav" },
            { id: 8, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 9, name: "rne", sound: "/assets/sylls/rne.wav" },
            { id: 10, name: "rre", sound: "/assets/sylls/rre.wav" },
            { id: 11, name: "wo", sound: "/assets/sylls/wo.wav" },
            { id: 12, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 13, name: "na", sound: "/assets/sylls/na.wav" }
        ]
    },
    {
        id: 56,
        name: "MIXPRE-173-20200415-EK-list-orig_149",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_149.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 1, name: "ngu", sound: "/assets/sylls/ngu.wav" },
            { id: 2, name: "ma", sound: "/assets/sylls/ma.wav" },
            { id: 3, name: "wa", sound: "/assets/sylls/wa.wav" },
            { id: 4, name: "ngan", sound: "/assets/sylls/ngan.wav" },
            { id: 5, name: "ngun", sound: "/assets/sylls/ngun.wav" },
            { id: 6, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 7, name: "na", sound: "/assets/sylls/na.wav" },
            { id: 8, name: "ne", sound: "/assets/sylls/ne.wav" },
            { id: 9, name: "wo", sound: "/assets/sylls/wo.wav" },
            { id: 10, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 11, name: "won", sound: "/assets/sylls/won.wav" },
            { id: 12, name: "rne", sound: "/assets/sylls/rne.wav" }
        ]
    },
    {
        id: 57,
        name: "MIXPRE-173-20200415-EK-list-orig_15",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_15.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 1, name: "karr", sound: "/assets/sylls/karr.wav" },
            { id: 2, name: "rre", sound: "/assets/sylls/rre.wav" },
            { id: 3, name: "rri", sound: "/assets/sylls/rri.wav" }
        ]
    },
    {
        id: 58,
        name: "MIXPRE-173-20200415-EK-list-orig_150",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_150.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ma", sound: "/assets/sylls/ma.wav" },
            { id: 1, name: "me", sound: "/assets/sylls/me.wav" },
            { id: 2, name: "man", sound: "/assets/sylls/man.wav" },
            { id: 3, name: "marn", sound: "/assets/sylls/marn.wav" },
            { id: 4, name: "men", sound: "/assets/sylls/men.wav" },
            { id: 5, name: "bu", sound: "/assets/sylls/bu.wav" },
            { id: 6, name: "ku", sound: "/assets/sylls/ku.wav" }
        ]
    },
    {
        id: 59,
        name: "MIXPRE-173-20200415-EK-list-orig_151",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_151.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ma", sound: "/assets/sylls/ma.wav" },
            { id: 1, name: "me", sound: "/assets/sylls/me.wav" },
            { id: 2, name: "wa", sound: "/assets/sylls/wa.wav" },
            { id: 3, name: "man", sound: "/assets/sylls/man.wav" },
            { id: 4, name: "marn", sound: "/assets/sylls/marn.wav" },
            { id: 5, name: "men", sound: "/assets/sylls/men.wav" },
            { id: 6, name: "ngu", sound: "/assets/sylls/ngu.wav" },
            { id: 7, name: "ku", sound: "/assets/sylls/ku.wav" },
            { id: 8, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 9, name: "bu", sound: "/assets/sylls/bu.wav" }
        ]
    },
    {
        id: 60,
        name: "MIXPRE-173-20200415-EK-list-orig_152",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_152.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ma", sound: "/assets/sylls/ma.wav" },
            { id: 1, name: "me", sound: "/assets/sylls/me.wav" },
            { id: 2, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 3, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 4, name: "wa", sound: "/assets/sylls/wa.wav" },
            { id: 5, name: "man", sound: "/assets/sylls/man.wav" },
            { id: 6, name: "men", sound: "/assets/sylls/men.wav" },
            { id: 7, name: "ngan", sound: "/assets/sylls/ngan.wav" },
            { id: 8, name: "kan", sound: "/assets/sylls/kan.wav" },
            { id: 9, name: "ngu", sound: "/assets/sylls/ngu.wav" },
            { id: 10, name: "ku", sound: "/assets/sylls/ku.wav" }
        ]
    },
    {
        id: 61,
        name: "MIXPRE-173-20200415-EK-list-orig_153",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_153.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ma", sound: "/assets/sylls/ma.wav" },
            { id: 1, name: "me", sound: "/assets/sylls/me.wav" },
            { id: 2, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 3, name: "ba", sound: "/assets/sylls/ba.wav" },
            { id: 4, name: "na", sound: "/assets/sylls/na.wav" },
            { id: 5, name: "man", sound: "/assets/sylls/man.wav" },
            { id: 6, name: "men", sound: "/assets/sylls/men.wav" },
            { id: 7, name: "ngan", sound: "/assets/sylls/ngan.wav" },
            { id: 8, name: "ben", sound: "/assets/sylls/ben.wav" },
            { id: 9, name: "ku", sound: "/assets/sylls/ku.wav" },
            { id: 10, name: "ngu", sound: "/assets/sylls/ngu.wav" },
            { id: 11, name: "wa", sound: "/assets/sylls/wa.wav" },
            { id: 12, name: "ka", sound: "/assets/sylls/ka.wav" }
        ]
    },
    {
        id: 62,
        name: "MIXPRE-173-20200415-EK-list-orig_154",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_154.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 1, name: "ne", sound: "/assets/sylls/ne.wav" },
            { id: 2, name: "na", sound: "/assets/sylls/na.wav" },
            { id: 3, name: "rne", sound: "/assets/sylls/rne.wav" },
            { id: 4, name: "yi", sound: "/assets/sylls/yi.wav" },
            { id: 5, name: "bu", sound: "/assets/sylls/bu.wav" },
            { id: 6, name: "be", sound: "/assets/sylls/be.wav" },
            { id: 7, name: "bi", sound: "/assets/sylls/bi.wav" },
            { id: 8, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 9, name: "ku", sound: "/assets/sylls/ku.wav" },
            { id: 10, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 11, name: "ba", sound: "/assets/sylls/ba.wav" },
            { id: 12, name: "kan", sound: "/assets/sylls/kan.wav" }
        ]
    },
    {
        id: 63,
        name: "MIXPRE-173-20200415-EK-list-orig_155",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_155.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 1, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 2, name: "ma", sound: "/assets/sylls/ma.wav" },
            { id: 3, name: "kan", sound: "/assets/sylls/kan.wav" },
            { id: 4, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 5, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 6, name: "bi", sound: "/assets/sylls/bi.wav" },
            { id: 7, name: "bu", sound: "/assets/sylls/bu.wav" },
            { id: 8, name: "ku", sound: "/assets/sylls/ku.wav" },
            { id: 9, name: "kuk", sound: "/assets/sylls/kuk.wav" },
            { id: 10, name: "ke", sound: "/assets/sylls/ke.wav" },
            { id: 11, name: "ne", sound: "/assets/sylls/ne.wav" },
            { id: 12, name: "mi", sound: "/assets/sylls/mi.wav" },
            { id: 13, name: "me", sound: "/assets/sylls/me.wav" }
        ]
    },
    {
        id: 64,
        name: "MIXPRE-173-20200415-EK-list-orig_156",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_156.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ne", sound: "/assets/sylls/ne.wav" },
            { id: 1, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 2, name: "rne", sound: "/assets/sylls/rne.wav" },
            { id: 3, name: "bu", sound: "/assets/sylls/bu.wav" },
            { id: 4, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 5, name: "kan", sound: "/assets/sylls/kan.wav" },
            { id: 6, name: "mi", sound: "/assets/sylls/mi.wav" }
        ]
    },
    {
        id: 65,
        name: "MIXPRE-173-20200415-EK-list-orig_157",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_157.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 1, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 2, name: "bu", sound: "/assets/sylls/bu.wav" },
            { id: 3, name: "be", sound: "/assets/sylls/be.wav" },
            { id: 4, name: "ku", sound: "/assets/sylls/ku.wav" },
            { id: 5, name: "kuk", sound: "/assets/sylls/kuk.wav" },
            { id: 6, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 7, name: "ke", sound: "/assets/sylls/ke.wav" },
            { id: 8, name: "kan", sound: "/assets/sylls/kan.wav" }
        ]
    },
    {
        id: 66,
        name: "MIXPRE-173-20200415-EK-list-orig_158",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_158.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 1, name: "ne", sound: "/assets/sylls/ne.wav" },
            { id: 2, name: "na", sound: "/assets/sylls/na.wav" },
            { id: 3, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 4, name: "bu", sound: "/assets/sylls/bu.wav" },
            { id: 5, name: "be", sound: "/assets/sylls/be.wav" },
            { id: 6, name: "bi", sound: "/assets/sylls/bi.wav" },
            { id: 7, name: "ku", sound: "/assets/sylls/ku.wav" },
            { id: 8, name: "kuk", sound: "/assets/sylls/kuk.wav" },
            { id: 9, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 10, name: "kan", sound: "/assets/sylls/kan.wav" },
            { id: 11, name: "mi", sound: "/assets/sylls/mi.wav" }
        ]
    },
    {
        id: 67,
        name: "MIXPRE-173-20200415-EK-list-orig_159",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_159.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 1, name: "ma", sound: "/assets/sylls/ma.wav" },
            { id: 2, name: "na", sound: "/assets/sylls/na.wav" },
            { id: 3, name: "ngan", sound: "/assets/sylls/ngan.wav" },
            { id: 4, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 5, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 6, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 7, name: "bu", sound: "/assets/sylls/bu.wav" },
            { id: 8, name: "bi", sound: "/assets/sylls/bi.wav" },
            { id: 9, name: "be", sound: "/assets/sylls/be.wav" },
            { id: 10, name: "ke", sound: "/assets/sylls/ke.wav" },
            { id: 11, name: "ba", sound: "/assets/sylls/ba.wav" },
            { id: 12, name: "kan", sound: "/assets/sylls/kan.wav" },
            { id: 13, name: "ben", sound: "/assets/sylls/ben.wav" }
        ]
    },
    {
        id: 68,
        name: "MIXPRE-173-20200415-EK-list-orig_16",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_16.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 1, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 2, name: "kan", sound: "/assets/sylls/kan.wav" },
            { id: 3, name: "karr", sound: "/assets/sylls/karr.wav" },
            { id: 4, name: "djal", sound: "/assets/sylls/djal.wav" },
            { id: 5, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 6, name: "rre", sound: "/assets/sylls/rre.wav" },
            { id: 7, name: "di", sound: "/assets/sylls/di.wav" }
        ]
    },
    {
        id: 69,
        name: "MIXPRE-173-20200415-EK-list-orig_160",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_160.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 1, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 2, name: "ba", sound: "/assets/sylls/ba.wav" },
            { id: 3, name: "ma", sound: "/assets/sylls/ma.wav" },
            { id: 4, name: "wa", sound: "/assets/sylls/wa.wav" },
            { id: 5, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 6, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 7, name: "wo", sound: "/assets/sylls/wo.wav" },
            { id: 8, name: "bu", sound: "/assets/sylls/bu.wav" },
            { id: 9, name: "rre", sound: "/assets/sylls/rre.wav" },
            { id: 10, name: "rren", sound: "/assets/sylls/rren.wav" }
        ]
    },
    {
        id: 70,
        name: "MIXPRE-173-20200415-EK-list-orig_161",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_161.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 1, name: "ba", sound: "/assets/sylls/ba.wav" },
            { id: 2, name: "wa", sound: "/assets/sylls/wa.wav" },
            { id: 3, name: "karr", sound: "/assets/sylls/karr.wav" },
            { id: 4, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 5, name: "rre", sound: "/assets/sylls/rre.wav" },
            { id: 6, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 7, name: "wo", sound: "/assets/sylls/wo.wav" },
            { id: 8, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 9, name: "ngu", sound: "/assets/sylls/ngu.wav" }
        ]
    },
    {
        id: 71,
        name: "MIXPRE-173-20200415-EK-list-orig_162",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_162.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 1, name: "kan", sound: "/assets/sylls/kan.wav" },
            { id: 2, name: "ba", sound: "/assets/sylls/ba.wav" },
            { id: 3, name: "be", sound: "/assets/sylls/be.wav" },
            { id: 4, name: "bi", sound: "/assets/sylls/bi.wav" },
            { id: 5, name: "bu", sound: "/assets/sylls/bu.wav" },
            { id: 6, name: "ma", sound: "/assets/sylls/ma.wav" },
            { id: 7, name: "me", sound: "/assets/sylls/me.wav" },
            { id: 8, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 9, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 10, name: "na", sound: "/assets/sylls/na.wav" },
            { id: 11, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 12, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 13, name: "wa", sound: "/assets/sylls/wa.wav" },
            { id: 14, name: "wo", sound: "/assets/sylls/wo.wav" },
            { id: 15, name: "rre", sound: "/assets/sylls/rre.wav" },
            { id: 16, name: "rren", sound: "/assets/sylls/rren.wav" }
        ]
    },
    {
        id: 72,
        name: "MIXPRE-173-20200415-EK-list-orig_163",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_163.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "yi", sound: "/assets/sylls/yi.wav" },
            { id: 1, name: "wa", sound: "/assets/sylls/wa.wav" },
            { id: 2, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 3, name: "ya", sound: "/assets/sylls/ya.wav" },
            { id: 4, name: "bi", sound: "/assets/sylls/bi.wav" }
        ]
    },
    {
        id: 73,
        name: "MIXPRE-173-20200415-EK-list-orig_164",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_164.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "yi", sound: "/assets/sylls/yi.wav" },
            { id: 1, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 2, name: "wa", sound: "/assets/sylls/wa.wav" },
            { id: 3, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 4, name: "ma", sound: "/assets/sylls/ma.wav" },
            { id: 5, name: "ya", sound: "/assets/sylls/ya.wav" },
            { id: 6, name: "me", sound: "/assets/sylls/me.wav" }
        ]
    },
    {
        id: 74,
        name: "MIXPRE-173-20200415-EK-list-orig_165",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_165.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ya", sound: "/assets/sylls/ya.wav" },
            { id: 1, name: "yi", sound: "/assets/sylls/yi.wav" },
            { id: 2, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 3, name: "ni", sound: "/assets/sylls/ni.wav" }
        ]
    },
    {
        id: 75,
        name: "MIXPRE-173-20200415-EK-list-orig_166",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_166.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ke", sound: "/assets/sylls/ke.wav" },
            { id: 1, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 2, name: "be", sound: "/assets/sylls/be.wav" },
            { id: 3, name: "bi", sound: "/assets/sylls/bi.wav" },
            { id: 4, name: "kan", sound: "/assets/sylls/kan.wav" },
            { id: 5, name: "ben", sound: "/assets/sylls/ben.wav" },
            { id: 6, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 7, name: "ne", sound: "/assets/sylls/ne.wav" },
            { id: 8, name: "mi", sound: "/assets/sylls/mi.wav" },
            { id: 9, name: "me", sound: "/assets/sylls/me.wav" },
            { id: 10, name: "di", sound: "/assets/sylls/di.wav" }
        ]
    },
    {
        id: 76,
        name: "MIXPRE-173-20200415-EK-list-orig_167",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_167.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ke", sound: "/assets/sylls/ke.wav" },
            { id: 1, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 2, name: "bi", sound: "/assets/sylls/bi.wav" },
            { id: 3, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 4, name: "kan", sound: "/assets/sylls/kan.wav" },
            { id: 5, name: "ben", sound: "/assets/sylls/ben.wav" },
            { id: 6, name: "me", sound: "/assets/sylls/me.wav" },
            { id: 7, name: "mi", sound: "/assets/sylls/mi.wav" },
            { id: 8, name: "ma", sound: "/assets/sylls/ma.wav" },
            { id: 9, name: "ne", sound: "/assets/sylls/ne.wav" },
            { id: 10, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 11, name: "rne", sound: "/assets/sylls/rne.wav" }
        ]
    },
    {
        id: 77,
        name: "MIXPRE-173-20200415-EK-list-orig_168",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_168.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 1, name: "ba", sound: "/assets/sylls/ba.wav" },
            { id: 2, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 3, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 4, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 5, name: "wa", sound: "/assets/sylls/wa.wav" },
            { id: 6, name: "kan", sound: "/assets/sylls/kan.wav" },
            { id: 7, name: "ben", sound: "/assets/sylls/ben.wav" },
            { id: 8, name: "ne", sound: "/assets/sylls/ne.wav" },
            { id: 9, name: "mi", sound: "/assets/sylls/mi.wav" },
            { id: 10, name: "me", sound: "/assets/sylls/me.wav" }
        ]
    },
    {
        id: 78,
        name: "MIXPRE-173-20200415-EK-list-orig_169",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_169.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 1, name: "wa", sound: "/assets/sylls/wa.wav" },
            { id: 2, name: "kan", sound: "/assets/sylls/kan.wav" },
            { id: 3, name: "ma", sound: "/assets/sylls/ma.wav" },
            { id: 4, name: "me", sound: "/assets/sylls/me.wav" },
            { id: 5, name: "ba", sound: "/assets/sylls/ba.wav" },
            { id: 6, name: "na", sound: "/assets/sylls/na.wav" },
            { id: 7, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 8, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 9, name: "bo", sound: "/assets/sylls/bo.wav" },
            { id: 10, name: "ku", sound: "/assets/sylls/ku.wav" },
            { id: 11, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 12, name: "bi", sound: "/assets/sylls/bi.wav" },
            { id: 13, name: "djal", sound: "/assets/sylls/djal.wav" },
            { id: 14, name: "bu", sound: "/assets/sylls/bu.wav" },
            { id: 15, name: "ben", sound: "/assets/sylls/ben.wav" },
            { id: 16, name: "mi", sound: "/assets/sylls/mi.wav" },
            { id: 17, name: "ni", sound: "/assets/sylls/ni.wav" }
        ]
    },
    {
        id: 79,
        name: "MIXPRE-173-20200415-EK-list-orig_17",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_17.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 1, name: "ba", sound: "/assets/sylls/ba.wav" },
            { id: 2, name: "wa", sound: "/assets/sylls/wa.wav" },
            { id: 3, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 4, name: "ma", sound: "/assets/sylls/ma.wav" },
            { id: 5, name: "kan", sound: "/assets/sylls/kan.wav" },
            { id: 6, name: "ben", sound: "/assets/sylls/ben.wav" },
            { id: 7, name: "ngan", sound: "/assets/sylls/ngan.wav" },
            { id: 8, name: "men", sound: "/assets/sylls/men.wav" },
            { id: 9, name: "bu", sound: "/assets/sylls/bu.wav" },
            { id: 10, name: "bi", sound: "/assets/sylls/bi.wav" },
            { id: 11, name: "ku", sound: "/assets/sylls/ku.wav" }
        ]
    },
    {
        id: 80,
        name: "MIXPRE-173-20200415-EK-list-orig_170",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_170.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 1, name: "na", sound: "/assets/sylls/na.wav" },
            { id: 2, name: "ma", sound: "/assets/sylls/ma.wav" },
            { id: 3, name: "wa", sound: "/assets/sylls/wa.wav" },
            { id: 4, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 5, name: "rre", sound: "/assets/sylls/rre.wav" },
            { id: 6, name: "nge", sound: "/assets/sylls/nge.wav" },
            { id: 7, name: "bu", sound: "/assets/sylls/bu.wav" },
            { id: 8, name: "ku", sound: "/assets/sylls/ku.wav" },
            { id: 9, name: "ngu", sound: "/assets/sylls/ngu.wav" }
        ]
    },
    {
        id: 81,
        name: "MIXPRE-173-20200415-EK-list-orig_171",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_171.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ne", sound: "/assets/sylls/ne.wav" },
            { id: 1, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 2, name: "rre", sound: "/assets/sylls/rre.wav" },
            { id: 3, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 4, name: "rne", sound: "/assets/sylls/rne.wav" },
            { id: 5, name: "nge", sound: "/assets/sylls/nge.wav" },
            { id: 6, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 7, name: "bu", sound: "/assets/sylls/bu.wav" },
            { id: 8, name: "ku", sound: "/assets/sylls/ku.wav" }
        ]
    },
    {
        id: 82,
        name: "MIXPRE-173-20200415-EK-list-orig_172",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_172.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ma", sound: "/assets/sylls/ma.wav" },
            { id: 1, name: "me", sound: "/assets/sylls/me.wav" },
            { id: 2, name: "mi", sound: "/assets/sylls/mi.wav" }
        ]
    },
    {
        id: 83,
        name: "MIXPRE-173-20200415-EK-list-orig_173",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_173.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 1, name: "ma", sound: "/assets/sylls/ma.wav" },
            { id: 2, name: "ba", sound: "/assets/sylls/ba.wav" },
            { id: 3, name: "kan", sound: "/assets/sylls/kan.wav" },
            { id: 4, name: "man", sound: "/assets/sylls/man.wav" },
            { id: 5, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 6, name: "ngu", sound: "/assets/sylls/ngu.wav" },
            { id: 7, name: "me", sound: "/assets/sylls/me.wav" }
        ]
    },
    {
        id: 84,
        name: "MIXPRE-173-20200415-EK-list-orig_174",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_174.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 1, name: "ke", sound: "/assets/sylls/ke.wav" },
            { id: 2, name: "be", sound: "/assets/sylls/be.wav" },
            { id: 3, name: "bi", sound: "/assets/sylls/bi.wav" },
            { id: 4, name: "rre", sound: "/assets/sylls/rre.wav" },
            { id: 5, name: "ben", sound: "/assets/sylls/ben.wav" },
            { id: 6, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 7, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 8, name: "kan", sound: "/assets/sylls/kan.wav" },
            { id: 9, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 10, name: "na", sound: "/assets/sylls/na.wav" },
            { id: 11, name: "ngan", sound: "/assets/sylls/ngan.wav" }
        ]
    },
    {
        id: 85,
        name: "MIXPRE-173-20200415-EK-list-orig_175",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_175.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "rre", sound: "/assets/sylls/rre.wav" },
            { id: 1, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 2, name: "be", sound: "/assets/sylls/be.wav" },
            { id: 3, name: "bi", sound: "/assets/sylls/bi.wav" },
            { id: 4, name: "ke", sound: "/assets/sylls/ke.wav" },
            { id: 5, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 6, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 7, name: "me", sound: "/assets/sylls/me.wav" },
            { id: 8, name: "ben", sound: "/assets/sylls/ben.wav" },
            { id: 9, name: "meng", sound: "/assets/sylls/meng.wav" },
            { id: 10, name: "men", sound: "/assets/sylls/men.wav" },
            { id: 11, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 12, name: "na", sound: "/assets/sylls/na.wav" },
            { id: 13, name: "ma", sound: "/assets/sylls/ma.wav" }
        ]
    },
    {
        id: 86,
        name: "MIXPRE-173-20200415-EK-list-orig_176",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_176.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 1, name: "wa", sound: "/assets/sylls/wa.wav" },
            { id: 2, name: "ke", sound: "/assets/sylls/ke.wav" },
            { id: 3, name: "kan", sound: "/assets/sylls/kan.wav" },
            { id: 4, name: "na", sound: "/assets/sylls/na.wav" },
            { id: 5, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 6, name: "ma", sound: "/assets/sylls/ma.wav" },
            { id: 7, name: "me", sound: "/assets/sylls/me.wav" }
        ]
    },
    {
        id: 87,
        name: "MIXPRE-173-20200415-EK-list-orig_177",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_177.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 1, name: "ba", sound: "/assets/sylls/ba.wav" },
            { id: 2, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 3, name: "wa", sound: "/assets/sylls/wa.wav" },
            { id: 4, name: "karr", sound: "/assets/sylls/karr.wav" },
            { id: 5, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 6, name: "rre", sound: "/assets/sylls/rre.wav" },
            { id: 7, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 8, name: "ku", sound: "/assets/sylls/ku.wav" },
            { id: 9, name: "bu", sound: "/assets/sylls/bu.wav" },
            { id: 10, name: "be", sound: "/assets/sylls/be.wav" },
            { id: 11, name: "bi", sound: "/assets/sylls/bi.wav" },
            { id: 12, name: "ke", sound: "/assets/sylls/ke.wav" },
            { id: 13, name: "ben", sound: "/assets/sylls/ben.wav" },
            { id: 14, name: "kan", sound: "/assets/sylls/kan.wav" },
            { id: 15, name: "ni", sound: "/assets/sylls/ni.wav" }
        ]
    },
    {
        id: 88,
        name: "MIXPRE-173-20200415-EK-list-orig_178",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_178.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 1, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 2, name: "na", sound: "/assets/sylls/na.wav" },
            { id: 3, name: "karr", sound: "/assets/sylls/karr.wav" },
            { id: 4, name: "rre", sound: "/assets/sylls/rre.wav" },
            { id: 5, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 6, name: "ku", sound: "/assets/sylls/ku.wav" },
            { id: 7, name: "mi", sound: "/assets/sylls/mi.wav" },
            { id: 8, name: "me", sound: "/assets/sylls/me.wav" },
            { id: 9, name: "ma", sound: "/assets/sylls/ma.wav" },
            { id: 10, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 11, name: "men", sound: "/assets/sylls/men.wav" },
            { id: 12, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 13, name: "bi", sound: "/assets/sylls/bi.wav" }
        ]
    },
    {
        id: 89,
        name: "MIXPRE-173-20200415-EK-list-orig_179",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_179.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ya", sound: "/assets/sylls/ya.wav" },
            { id: 1, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 2, name: "wa", sound: "/assets/sylls/wa.wav" },
            { id: 3, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 4, name: "rre", sound: "/assets/sylls/rre.wav" },
            { id: 5, name: "di", sound: "/assets/sylls/di.wav" }
        ]
    },
    {
        id: 90,
        name: "MIXPRE-173-20200415-EK-list-orig_18",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_18.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ma", sound: "/assets/sylls/ma.wav" },
            { id: 1, name: "me", sound: "/assets/sylls/me.wav" },
            { id: 2, name: "bi", sound: "/assets/sylls/bi.wav" },
            { id: 3, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 4, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 5, name: "di", sound: "/assets/sylls/di.wav" }
        ]
    },
    {
        id: 91,
        name: "MIXPRE-173-20200415-EK-list-orig_180",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_180.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ya", sound: "/assets/sylls/ya.wav" },
            { id: 1, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 2, name: "kan", sound: "/assets/sylls/kan.wav" },
            { id: 3, name: "wa", sound: "/assets/sylls/wa.wav" },
            { id: 4, name: "wo", sound: "/assets/sylls/wo.wav" },
            { id: 5, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 6, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 7, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 8, name: "ni", sound: "/assets/sylls/ni.wav" }
        ]
    },
    {
        id: 92,
        name: "MIXPRE-173-20200415-EK-list-orig_181",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_181.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ya", sound: "/assets/sylls/ya.wav" },
            { id: 1, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 2, name: "na", sound: "/assets/sylls/na.wav" },
            { id: 3, name: "wa", sound: "/assets/sylls/wa.wav" },
            { id: 4, name: "bu", sound: "/assets/sylls/bu.wav" },
            { id: 5, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 6, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 7, name: "ni", sound: "/assets/sylls/ni.wav" }
        ]
    },
    {
        id: 93,
        name: "MIXPRE-173-20200415-EK-list-orig_182",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_182.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 1, name: "na", sound: "/assets/sylls/na.wav" },
            { id: 2, name: "ma", sound: "/assets/sylls/ma.wav" },
            { id: 3, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 4, name: "kan", sound: "/assets/sylls/kan.wav" },
            { id: 5, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 6, name: "rre", sound: "/assets/sylls/rre.wav" },
            { id: 7, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 8, name: "ku", sound: "/assets/sylls/ku.wav" },
            { id: 9, name: "bu", sound: "/assets/sylls/bu.wav" }
        ]
    },
    {
        id: 94,
        name: "MIXPRE-173-20200415-EK-list-orig_183",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_183.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "na", sound: "/assets/sylls/na.wav" },
            { id: 1, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 2, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 3, name: "ma", sound: "/assets/sylls/ma.wav" },
            { id: 4, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 5, name: "rre", sound: "/assets/sylls/rre.wav" },
            { id: 6, name: "ku", sound: "/assets/sylls/ku.wav" },
            { id: 7, name: "ngu", sound: "/assets/sylls/ngu.wav" },
            { id: 8, name: "kan", sound: "/assets/sylls/kan.wav" },
            { id: 9, name: "djal", sound: "/assets/sylls/djal.wav" },
            { id: 10, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 11, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 12, name: "mi", sound: "/assets/sylls/mi.wav" },
            { id: 13, name: "me", sound: "/assets/sylls/me.wav" }
        ]
    },
    {
        id: 95,
        name: "MIXPRE-173-20200415-EK-list-orig_184",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_184.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 1, name: "ke", sound: "/assets/sylls/ke.wav" },
            { id: 2, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 3, name: "kan", sound: "/assets/sylls/kan.wav" },
            { id: 4, name: "ngan", sound: "/assets/sylls/ngan.wav" },
            { id: 5, name: "kenh", sound: "/assets/sylls/kenh.wav" },
            { id: 6, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 7, name: "bi", sound: "/assets/sylls/bi.wav" },
            { id: 8, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 9, name: "wa", sound: "/assets/sylls/wa.wav" }
        ]
    },
    {
        id: 96,
        name: "MIXPRE-173-20200415-EK-list-orig_185",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_185.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 1, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 2, name: "kan", sound: "/assets/sylls/kan.wav" },
            { id: 3, name: "ngan", sound: "/assets/sylls/ngan.wav" },
            { id: 4, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 5, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 6, name: "ya", sound: "/assets/sylls/ya.wav" },
            { id: 7, name: "wa", sound: "/assets/sylls/wa.wav" }
        ]
    },
    {
        id: 97,
        name: "MIXPRE-173-20200415-EK-list-orig_186",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_186.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 1, name: "ke", sound: "/assets/sylls/ke.wav" },
            { id: 2, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 3, name: "kan", sound: "/assets/sylls/kan.wav" },
            { id: 4, name: "ngan", sound: "/assets/sylls/ngan.wav" },
            { id: 5, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 6, name: "bi", sound: "/assets/sylls/bi.wav" },
            { id: 7, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 8, name: "dja", sound: "/assets/sylls/dja.wav" },
            { id: 9, name: "ya", sound: "/assets/sylls/ya.wav" },
            { id: 10, name: "wa", sound: "/assets/sylls/wa.wav" },
            { id: 11, name: "wo", sound: "/assets/sylls/wo.wav" }
        ]
    },
    {
        id: 98,
        name: "MIXPRE-173-20200415-EK-list-orig_187",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_187.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 1, name: "ke", sound: "/assets/sylls/ke.wav" },
            { id: 2, name: "kan", sound: "/assets/sylls/kan.wav" },
            { id: 3, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 4, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 5, name: "mi", sound: "/assets/sylls/mi.wav" },
            { id: 6, name: "wa", sound: "/assets/sylls/wa.wav" }
        ]
    },
    {
        id: 99,
        name: "MIXPRE-173-20200415-EK-list-orig_188",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_188.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 1, name: "ke", sound: "/assets/sylls/ke.wav" },
            { id: 2, name: "wa", sound: "/assets/sylls/wa.wav" },
            { id: 3, name: "kan", sound: "/assets/sylls/kan.wav" },
            { id: 4, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 5, name: "bi", sound: "/assets/sylls/bi.wav" },
            { id: 6, name: "ya", sound: "/assets/sylls/ya.wav" },
            { id: 7, name: "wo", sound: "/assets/sylls/wo.wav" }
        ]
    },
    {
        id: 100,
        name: "MIXPRE-173-20200415-EK-list-orig_189",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_189.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 1, name: "bi", sound: "/assets/sylls/bi.wav" },
            { id: 2, name: "be", sound: "/assets/sylls/be.wav" },
            { id: 3, name: "ba", sound: "/assets/sylls/ba.wav" },
            { id: 4, name: "bu", sound: "/assets/sylls/bu.wav" },
            { id: 5, name: "ben", sound: "/assets/sylls/ben.wav" },
            { id: 6, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 7, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 8, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 9, name: "yi", sound: "/assets/sylls/yi.wav" },
            { id: 10, name: "me", sound: "/assets/sylls/me.wav" },
            { id: 11, name: "mi", sound: "/assets/sylls/mi.wav" },
            { id: 12, name: "ma", sound: "/assets/sylls/ma.wav" }
        ]
    },
    {
        id: 101,
        name: "MIXPRE-173-20200415-EK-list-orig_19",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_19.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "be", sound: "/assets/sylls/be.wav" },
            { id: 1, name: "bi", sound: "/assets/sylls/bi.wav" },
            { id: 2, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 3, name: "ke", sound: "/assets/sylls/ke.wav" },
            { id: 4, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 5, name: "ben", sound: "/assets/sylls/ben.wav" },
            { id: 6, name: "kan", sound: "/assets/sylls/kan.wav" },
            { id: 7, name: "na", sound: "/assets/sylls/na.wav" },
            { id: 8, name: "wa", sound: "/assets/sylls/wa.wav" },
            { id: 9, name: "wo", sound: "/assets/sylls/wo.wav" }
        ]
    },
    {
        id: 102,
        name: "MIXPRE-173-20200415-EK-list-orig_190",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_190.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 1, name: "ke", sound: "/assets/sylls/ke.wav" },
            { id: 2, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 3, name: "bi", sound: "/assets/sylls/bi.wav" },
            { id: 4, name: "rre", sound: "/assets/sylls/rre.wav" },
            { id: 5, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 6, name: "ben", sound: "/assets/sylls/ben.wav" },
            { id: 7, name: "rren", sound: "/assets/sylls/rren.wav" },
            { id: 8, name: "ne", sound: "/assets/sylls/ne.wav" },
            { id: 9, name: "me", sound: "/assets/sylls/me.wav" },
            { id: 10, name: "rne", sound: "/assets/sylls/rne.wav" }
        ]
    },
    {
        id: 103,
        name: "MIXPRE-173-20200415-EK-list-orig_191",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_191.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 1, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 2, name: "karr", sound: "/assets/sylls/karr.wav" },
            { id: 3, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 4, name: "rre", sound: "/assets/sylls/rre.wav" },
            { id: 5, name: "bo", sound: "/assets/sylls/bo.wav" },
            { id: 6, name: "bu", sound: "/assets/sylls/bu.wav" },
            { id: 7, name: "ngu", sound: "/assets/sylls/ngu.wav" },
            { id: 8, name: "wo", sound: "/assets/sylls/wo.wav" }
        ]
    },
    {
        id: 104,
        name: "MIXPRE-173-20200415-EK-list-orig_192",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_192.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 1, name: "ba", sound: "/assets/sylls/ba.wav" },
            { id: 2, name: "wa", sound: "/assets/sylls/wa.wav" },
            { id: 3, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 4, name: "karr", sound: "/assets/sylls/karr.wav" },
            { id: 5, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 6, name: "rre", sound: "/assets/sylls/rre.wav" },
            { id: 7, name: "ma", sound: "/assets/sylls/ma.wav" },
            { id: 8, name: "me", sound: "/assets/sylls/me.wav" },
            { id: 9, name: "meng", sound: "/assets/sylls/meng.wav" },
            { id: 10, name: "men", sound: "/assets/sylls/men.wav" },
            { id: 11, name: "bu", sound: "/assets/sylls/bu.wav" }
        ]
    },
    {
        id: 105,
        name: "MIXPRE-173-20200415-EK-list-orig_193",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_193.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 1, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 2, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 3, name: "rre", sound: "/assets/sylls/rre.wav" },
            { id: 4, name: "di", sound: "/assets/sylls/di.wav" }
        ]
    },
    {
        id: 106,
        name: "MIXPRE-173-20200415-EK-list-orig_194",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_194.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 1, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 2, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 3, name: "rre", sound: "/assets/sylls/rre.wav" },
            { id: 4, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 5, name: "ni", sound: "/assets/sylls/ni.wav" }
        ]
    },
    {
        id: 107,
        name: "MIXPRE-173-20200415-EK-list-orig_195",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_195.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ku", sound: "/assets/sylls/ku.wav" },
            { id: 1, name: "bu", sound: "/assets/sylls/bu.wav" },
            { id: 2, name: "bi", sound: "/assets/sylls/bi.wav" },
            { id: 3, name: "kun", sound: "/assets/sylls/kun.wav" },
            { id: 4, name: "bun", sound: "/assets/sylls/bun.wav" },
            { id: 5, name: "ben", sound: "/assets/sylls/ben.wav" }
        ]
    },
    {
        id: 108,
        name: "MIXPRE-173-20200415-EK-list-orig_196",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_196.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ku", sound: "/assets/sylls/ku.wav" },
            { id: 1, name: "ke", sound: "/assets/sylls/ke.wav" },
            { id: 2, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 3, name: "bu", sound: "/assets/sylls/bu.wav" },
            { id: 4, name: "kun", sound: "/assets/sylls/kun.wav" },
            { id: 5, name: "kan", sound: "/assets/sylls/kan.wav" },
            { id: 6, name: "ben", sound: "/assets/sylls/ben.wav" },
            { id: 7, name: "djal", sound: "/assets/sylls/djal.wav" }
        ]
    },
    {
        id: 109,
        name: "MIXPRE-173-20200415-EK-list-orig_197",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_197.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 1, name: "ke", sound: "/assets/sylls/ke.wav" },
            { id: 2, name: "yi", sound: "/assets/sylls/yi.wav" },
            { id: 3, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 4, name: "bi", sound: "/assets/sylls/bi.wav" },
            { id: 5, name: "bu", sound: "/assets/sylls/bu.wav" }
        ]
    },
    {
        id: 110,
        name: "MIXPRE-173-20200415-EK-list-orig_198",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_198.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ya", sound: "/assets/sylls/ya.wav" },
            { id: 1, name: "yi", sound: "/assets/sylls/yi.wav" },
            { id: 2, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 3, name: "ke", sound: "/assets/sylls/ke.wav" },
            { id: 4, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 5, name: "be", sound: "/assets/sylls/be.wav" },
            { id: 6, name: "bi", sound: "/assets/sylls/bi.wav" },
            { id: 7, name: "me", sound: "/assets/sylls/me.wav" }
        ]
    },
    {
        id: 111,
        name: "MIXPRE-173-20200415-EK-list-orig_199",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_199.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 1, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 2, name: "bi", sound: "/assets/sylls/bi.wav" }
        ]
    },
    {
        id: 112,
        name: "MIXPRE-173-20200415-EK-list-orig_2",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_2.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 1, name: "ke", sound: "/assets/sylls/ke.wav" },
            { id: 2, name: "kan", sound: "/assets/sylls/kan.wav" },
            { id: 3, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 4, name: "ne", sound: "/assets/sylls/ne.wav" },
            { id: 5, name: "na", sound: "/assets/sylls/na.wav" }
        ]
    },
    {
        id: 113,
        name: "MIXPRE-173-20200415-EK-list-orig_20",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_20.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "wa", sound: "/assets/sylls/wa.wav" },
            { id: 1, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 2, name: "ma", sound: "/assets/sylls/ma.wav" },
            { id: 3, name: "me", sound: "/assets/sylls/me.wav" },
            { id: 4, name: "ngan", sound: "/assets/sylls/ngan.wav" },
            { id: 5, name: "men", sound: "/assets/sylls/men.wav" },
            { id: 6, name: "ben", sound: "/assets/sylls/ben.wav" },
            { id: 7, name: "na", sound: "/assets/sylls/na.wav" },
            { id: 8, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 9, name: "ke", sound: "/assets/sylls/ke.wav" },
            { id: 10, name: "dja", sound: "/assets/sylls/dja.wav" },
            { id: 11, name: "ya", sound: "/assets/sylls/ya.wav" },
            { id: 12, name: "wo", sound: "/assets/sylls/wo.wav" },
            { id: 13, name: "ba", sound: "/assets/sylls/ba.wav" }
        ]
    },
    {
        id: 114,
        name: "MIXPRE-173-20200415-EK-list-orig_200",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_200.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 1, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 2, name: "wa", sound: "/assets/sylls/wa.wav" },
            { id: 3, name: "kan", sound: "/assets/sylls/kan.wav" },
            { id: 4, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 5, name: "bi", sound: "/assets/sylls/bi.wav" }
        ]
    },
    {
        id: 115,
        name: "MIXPRE-173-20200415-EK-list-orig_201",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_201.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 1, name: "karr", sound: "/assets/sylls/karr.wav" },
            { id: 2, name: "rre", sound: "/assets/sylls/rre.wav" },
            { id: 3, name: "rren", sound: "/assets/sylls/rren.wav" },
            { id: 4, name: "kan", sound: "/assets/sylls/kan.wav" },
            { id: 5, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 6, name: "ngu", sound: "/assets/sylls/ngu.wav" },
            { id: 7, name: "di", sound: "/assets/sylls/di.wav" }
        ]
    },
    {
        id: 116,
        name: "MIXPRE-173-20200415-EK-list-orig_202",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_202.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ka", sound: "/assets/sylls/ka.wav" }
        ]
    },
    {
        id: 117,
        name: "MIXPRE-173-20200415-EK-list-orig_203",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_203.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ma", sound: "/assets/sylls/ma.wav" },
            { id: 1, name: "me", sound: "/assets/sylls/me.wav" },
            { id: 2, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 3, name: "ba", sound: "/assets/sylls/ba.wav" },
            { id: 4, name: "wa", sound: "/assets/sylls/wa.wav" },
            { id: 5, name: "bi", sound: "/assets/sylls/bi.wav" },
            { id: 6, name: "ke", sound: "/assets/sylls/ke.wav" },
            { id: 7, name: "djal", sound: "/assets/sylls/djal.wav" },
            { id: 8, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 9, name: "ka", sound: "/assets/sylls/ka.wav" }
        ]
    },
    {
        id: 118,
        name: "MIXPRE-173-20200415-EK-list-orig_204",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_204.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ma", sound: "/assets/sylls/ma.wav" },
            { id: 1, name: "me", sound: "/assets/sylls/me.wav" },
            { id: 2, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 3, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 4, name: "man", sound: "/assets/sylls/man.wav" },
            { id: 5, name: "men", sound: "/assets/sylls/men.wav" },
            { id: 6, name: "ngan", sound: "/assets/sylls/ngan.wav" },
            { id: 7, name: "kan", sound: "/assets/sylls/kan.wav" },
            { id: 8, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 9, name: "rne", sound: "/assets/sylls/rne.wav" },
            { id: 10, name: "yi", sound: "/assets/sylls/yi.wav" }
        ]
    },
    {
        id: 119,
        name: "MIXPRE-173-20200415-EK-list-orig_206",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_206.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 1, name: "ke", sound: "/assets/sylls/ke.wav" },
            { id: 2, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 3, name: "wa", sound: "/assets/sylls/wa.wav" },
            { id: 4, name: "kan", sound: "/assets/sylls/kan.wav" }
        ]
    },
    {
        id: 120,
        name: "MIXPRE-173-20200415-EK-list-orig_207",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_207.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ba", sound: "/assets/sylls/ba.wav" },
            { id: 1, name: "wa", sound: "/assets/sylls/wa.wav" },
            { id: 2, name: "ne", sound: "/assets/sylls/ne.wav" },
            { id: 3, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 4, name: "na", sound: "/assets/sylls/na.wav" },
            { id: 5, name: "rne", sound: "/assets/sylls/rne.wav" }
        ]
    },
    {
        id: 121,
        name: "MIXPRE-173-20200415-EK-list-orig_208",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_208.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 1, name: "rne", sound: "/assets/sylls/rne.wav" }
        ]
    },
    {
        id: 122,
        name: "MIXPRE-173-20200415-EK-list-orig_209",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_209.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 1, name: "kan", sound: "/assets/sylls/kan.wav" },
            { id: 2, name: "rri", sound: "/assets/sylls/rri.wav" }
        ]
    },
    {
        id: 123,
        name: "MIXPRE-173-20200415-EK-list-orig_21",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_21.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 1, name: "ma", sound: "/assets/sylls/ma.wav" },
            { id: 2, name: "me", sound: "/assets/sylls/me.wav" },
            { id: 3, name: "na", sound: "/assets/sylls/na.wav" },
            { id: 4, name: "ngan", sound: "/assets/sylls/ngan.wav" },
            { id: 5, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 6, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 7, name: "rre", sound: "/assets/sylls/rre.wav" },
            { id: 8, name: "ke", sound: "/assets/sylls/ke.wav" },
            { id: 9, name: "ku", sound: "/assets/sylls/ku.wav" },
            { id: 10, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 11, name: "bi", sound: "/assets/sylls/bi.wav" },
            { id: 12, name: "kan", sound: "/assets/sylls/kan.wav" },
            { id: 13, name: "ben", sound: "/assets/sylls/ben.wav" }
        ]
    },
    {
        id: 124,
        name: "MIXPRE-173-20200415-EK-list-orig_210",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_210.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 1, name: "nga", sound: "/assets/sylls/nga.wav" }
        ]
    },
    {
        id: 125,
        name: "MIXPRE-173-20200415-EK-list-orig_211",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_211.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ku", sound: "/assets/sylls/ku.wav" },
            { id: 1, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 2, name: "ke", sound: "/assets/sylls/ke.wav" },
            { id: 3, name: "bu", sound: "/assets/sylls/bu.wav" },
            { id: 4, name: "kuk", sound: "/assets/sylls/kuk.wav" }
        ]
    },
    {
        id: 126,
        name: "MIXPRE-173-20200415-EK-list-orig_212",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_212.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "bi", sound: "/assets/sylls/bi.wav" },
            { id: 1, name: "be", sound: "/assets/sylls/be.wav" },
            { id: 2, name: "bu", sound: "/assets/sylls/bu.wav" },
            { id: 3, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 4, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 5, name: "ben", sound: "/assets/sylls/ben.wav" },
            { id: 6, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 7, name: "ni", sound: "/assets/sylls/ni.wav" }
        ]
    },
    {
        id: 127,
        name: "MIXPRE-173-20200415-EK-list-orig_213",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_213.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "bi", sound: "/assets/sylls/bi.wav" },
            { id: 1, name: "bu", sound: "/assets/sylls/bu.wav" },
            { id: 2, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 3, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 4, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 5, name: "rrinj", sound: "/assets/sylls/rrinj.wav" },
            { id: 6, name: "yi", sound: "/assets/sylls/yi.wav" }
        ]
    },
    {
        id: 128,
        name: "MIXPRE-173-20200415-EK-list-orig_214",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_214.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 1, name: "ku", sound: "/assets/sylls/ku.wav" },
            { id: 2, name: "bi", sound: "/assets/sylls/bi.wav" },
            { id: 3, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 4, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 5, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 6, name: "karr", sound: "/assets/sylls/karr.wav" },
            { id: 7, name: "rre", sound: "/assets/sylls/rre.wav" },
            { id: 8, name: "rren", sound: "/assets/sylls/rren.wav" },
            { id: 9, name: "me", sound: "/assets/sylls/me.wav" },
            { id: 10, name: "ne", sound: "/assets/sylls/ne.wav" },
            { id: 11, name: "rne", sound: "/assets/sylls/rne.wav" },
            { id: 12, name: "meng", sound: "/assets/sylls/meng.wav" },
            { id: 13, name: "men", sound: "/assets/sylls/men.wav" }
        ]
    },
    {
        id: 129,
        name: "MIXPRE-173-20200415-EK-list-orig_215",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_215.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "bi", sound: "/assets/sylls/bi.wav" },
            { id: 1, name: "be", sound: "/assets/sylls/be.wav" },
            { id: 2, name: "ba", sound: "/assets/sylls/ba.wav" },
            { id: 3, name: "yi", sound: "/assets/sylls/yi.wav" },
            { id: 4, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 5, name: "ben", sound: "/assets/sylls/ben.wav" },
            { id: 6, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 7, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 8, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 9, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 10, name: "kan", sound: "/assets/sylls/kan.wav" },
            { id: 11, name: "ngan", sound: "/assets/sylls/ngan.wav" },
            { id: 12, name: "ne", sound: "/assets/sylls/ne.wav" },
            { id: 13, name: "rre", sound: "/assets/sylls/rre.wav" },
            { id: 14, name: "me", sound: "/assets/sylls/me.wav" },
            { id: 15, name: "mi", sound: "/assets/sylls/mi.wav" },
            { id: 16, name: "men", sound: "/assets/sylls/men.wav" },
            { id: 17, name: "meng", sound: "/assets/sylls/meng.wav" }
        ]
    },
    {
        id: 130,
        name: "MIXPRE-173-20200415-EK-list-orig_216",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_216.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 1, name: "karr", sound: "/assets/sylls/karr.wav" },
            { id: 2, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 3, name: "ke", sound: "/assets/sylls/ke.wav" },
            { id: 4, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 5, name: "kan", sound: "/assets/sylls/kan.wav" }
        ]
    },
    {
        id: 131,
        name: "MIXPRE-173-20200415-EK-list-orig_217",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_217.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 1, name: "karr", sound: "/assets/sylls/karr.wav" },
            { id: 2, name: "kan", sound: "/assets/sylls/kan.wav" },
            { id: 3, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 4, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 5, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 6, name: "rrih", sound: "/assets/sylls/rrih.wav" },
            { id: 7, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 8, name: "na", sound: "/assets/sylls/na.wav" }
        ]
    },
    {
        id: 132,
        name: "MIXPRE-173-20200415-EK-list-orig_218",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_218.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "bi", sound: "/assets/sylls/bi.wav" },
            { id: 1, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 2, name: "rre", sound: "/assets/sylls/rre.wav" },
            { id: 3, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 4, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 5, name: "ka", sound: "/assets/sylls/ka.wav" }
        ]
    },
    {
        id: 133,
        name: "MIXPRE-173-20200415-EK-list-orig_219",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_219.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "bi", sound: "/assets/sylls/bi.wav" },
            { id: 1, name: "be", sound: "/assets/sylls/be.wav" },
            { id: 2, name: "bu", sound: "/assets/sylls/bu.wav" },
            { id: 3, name: "yi", sound: "/assets/sylls/yi.wav" },
            { id: 4, name: "ben", sound: "/assets/sylls/ben.wav" },
            { id: 5, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 6, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 7, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 8, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 9, name: "ka", sound: "/assets/sylls/ka.wav" }
        ]
    },
    {
        id: 134,
        name: "MIXPRE-173-20200415-EK-list-orig_22",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_22.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 1, name: "karr", sound: "/assets/sylls/karr.wav" },
            { id: 2, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 3, name: "bi", sound: "/assets/sylls/bi.wav" }
        ]
    },
    {
        id: 135,
        name: "MIXPRE-173-20200415-EK-list-orig_222",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_222.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ke", sound: "/assets/sylls/ke.wav" },
            { id: 1, name: "bi", sound: "/assets/sylls/bi.wav" },
            { id: 2, name: "rre", sound: "/assets/sylls/rre.wav" },
            { id: 3, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 4, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 5, name: "rne", sound: "/assets/sylls/rne.wav" },
            { id: 6, name: "rren", sound: "/assets/sylls/rren.wav" },
            { id: 7, name: "ma", sound: "/assets/sylls/ma.wav" },
            { id: 8, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 9, name: "wa", sound: "/assets/sylls/wa.wav" },
            { id: 10, name: "na", sound: "/assets/sylls/na.wav" },
            { id: 11, name: "man", sound: "/assets/sylls/man.wav" },
            { id: 12, name: "marn", sound: "/assets/sylls/marn.wav" },
            { id: 13, name: "ngan", sound: "/assets/sylls/ngan.wav" },
            { id: 14, name: "ne", sound: "/assets/sylls/ne.wav" }
        ]
    },
    {
        id: 136,
        name: "MIXPRE-173-20200415-EK-list-orig_223",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_223.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "bi", sound: "/assets/sylls/bi.wav" },
            { id: 1, name: "be", sound: "/assets/sylls/be.wav" },
            { id: 2, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 3, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 4, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 5, name: "ma", sound: "/assets/sylls/ma.wav" },
            { id: 6, name: "man", sound: "/assets/sylls/man.wav" },
            { id: 7, name: "marn", sound: "/assets/sylls/marn.wav" }
        ]
    },
    {
        id: 137,
        name: "MIXPRE-173-20200415-EK-list-orig_224",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_224.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 1, name: "ke", sound: "/assets/sylls/ke.wav" },
            { id: 2, name: "bi", sound: "/assets/sylls/bi.wav" },
            { id: 3, name: "yi", sound: "/assets/sylls/yi.wav" },
            { id: 4, name: "kan", sound: "/assets/sylls/kan.wav" },
            { id: 5, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 6, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 7, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 8, name: "me", sound: "/assets/sylls/me.wav" },
            { id: 9, name: "ma", sound: "/assets/sylls/ma.wav" },
            { id: 10, name: "mi", sound: "/assets/sylls/mi.wav" },
            { id: 11, name: "men", sound: "/assets/sylls/men.wav" },
            { id: 12, name: "man", sound: "/assets/sylls/man.wav" },
            { id: 13, name: "ne", sound: "/assets/sylls/ne.wav" }
        ]
    },
    {
        id: 138,
        name: "MIXPRE-173-20200415-EK-list-orig_225",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_225.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 1, name: "kan", sound: "/assets/sylls/kan.wav" },
            { id: 2, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 3, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 4, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 5, name: "bu", sound: "/assets/sylls/bu.wav" },
            { id: 6, name: "bi", sound: "/assets/sylls/bi.wav" },
            { id: 7, name: "ku", sound: "/assets/sylls/ku.wav" },
            { id: 8, name: "bun", sound: "/assets/sylls/bun.wav" },
            { id: 9, name: "ne", sound: "/assets/sylls/ne.wav" }
        ]
    },
    {
        id: 139,
        name: "MIXPRE-173-20200415-EK-list-orig_226",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_226.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ku", sound: "/assets/sylls/ku.wav" },
            { id: 1, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 2, name: "ke", sound: "/assets/sylls/ke.wav" },
            { id: 3, name: "bu", sound: "/assets/sylls/bu.wav" },
            { id: 4, name: "kan", sound: "/assets/sylls/kan.wav" },
            { id: 5, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 6, name: "rre", sound: "/assets/sylls/rre.wav" },
            { id: 7, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 8, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 9, name: "kuk", sound: "/assets/sylls/kuk.wav" }
        ]
    },
    {
        id: 140,
        name: "MIXPRE-173-20200415-EK-list-orig_227",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_227.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "bi", sound: "/assets/sylls/bi.wav" },
            { id: 1, name: "be", sound: "/assets/sylls/be.wav" },
            { id: 2, name: "bu", sound: "/assets/sylls/bu.wav" },
            { id: 3, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 4, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 5, name: "rre", sound: "/assets/sylls/rre.wav" },
            { id: 6, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 7, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 8, name: "bo", sound: "/assets/sylls/bo.wav" },
            { id: 9, name: "ba", sound: "/assets/sylls/ba.wav" },
            { id: 10, name: "ke", sound: "/assets/sylls/ke.wav" }
        ]
    },
    {
        id: 141,
        name: "MIXPRE-173-20200415-EK-list-orig_228",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_228.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 1, name: "wa", sound: "/assets/sylls/wa.wav" },
            { id: 2, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 3, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 4, name: "rre", sound: "/assets/sylls/rre.wav" },
            { id: 5, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 6, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 7, name: "ku", sound: "/assets/sylls/ku.wav" },
            { id: 8, name: "bu", sound: "/assets/sylls/bu.wav" },
            { id: 9, name: "dja", sound: "/assets/sylls/dja.wav" },
            { id: 10, name: "djal", sound: "/assets/sylls/djal.wav" }
        ]
    },
    {
        id: 142,
        name: "MIXPRE-173-20200415-EK-list-orig_229",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_229.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "bi", sound: "/assets/sylls/bi.wav" },
            { id: 1, name: "be", sound: "/assets/sylls/be.wav" },
            { id: 2, name: "bu", sound: "/assets/sylls/bu.wav" },
            { id: 3, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 4, name: "rre", sound: "/assets/sylls/rre.wav" },
            { id: 5, name: "me", sound: "/assets/sylls/me.wav" },
            { id: 6, name: "ke", sound: "/assets/sylls/ke.wav" },
            { id: 7, name: "ka", sound: "/assets/sylls/ka.wav" }
        ]
    },
    {
        id: 143,
        name: "MIXPRE-173-20200415-EK-list-orig_23",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_23.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 1, name: "rre", sound: "/assets/sylls/rre.wav" },
            { id: 2, name: "bi", sound: "/assets/sylls/bi.wav" },
            { id: 3, name: "be", sound: "/assets/sylls/be.wav" },
            { id: 4, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 5, name: "mi", sound: "/assets/sylls/mi.wav" },
            { id: 6, name: "ben", sound: "/assets/sylls/ben.wav" },
            { id: 7, name: "me", sound: "/assets/sylls/me.wav" }
        ]
    },
    {
        id: 144,
        name: "MIXPRE-173-20200415-EK-list-orig_230",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_230.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 1, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 2, name: "wa", sound: "/assets/sylls/wa.wav" },
            { id: 3, name: "bi", sound: "/assets/sylls/bi.wav" },
            { id: 4, name: "be", sound: "/assets/sylls/be.wav" },
            { id: 5, name: "bu", sound: "/assets/sylls/bu.wav" },
            { id: 6, name: "ba", sound: "/assets/sylls/ba.wav" },
            { id: 7, name: "me", sound: "/assets/sylls/me.wav" },
            { id: 8, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 9, name: "ku", sound: "/assets/sylls/ku.wav" },
            { id: 10, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 11, name: "ke", sound: "/assets/sylls/ke.wav" }
        ]
    },
    {
        id: 145,
        name: "MIXPRE-173-20200415-EK-list-orig_231",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_231.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 1, name: "ba", sound: "/assets/sylls/ba.wav" },
            { id: 2, name: "bi", sound: "/assets/sylls/bi.wav" },
            { id: 3, name: "bu", sound: "/assets/sylls/bu.wav" },
            { id: 4, name: "be", sound: "/assets/sylls/be.wav" },
            { id: 5, name: "ben", sound: "/assets/sylls/ben.wav" },
            { id: 6, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 7, name: "rre", sound: "/assets/sylls/rre.wav" },
            { id: 8, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 9, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 10, name: "rren", sound: "/assets/sylls/rren.wav" },
            { id: 11, name: "na", sound: "/assets/sylls/na.wav" },
            { id: 12, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 13, name: "ngan", sound: "/assets/sylls/ngan.wav" },
            { id: 14, name: "kan", sound: "/assets/sylls/kan.wav" }
        ]
    },
    {
        id: 146,
        name: "MIXPRE-173-20200415-EK-list-orig_232",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_232.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 1, name: "bi", sound: "/assets/sylls/bi.wav" },
            { id: 2, name: "be", sound: "/assets/sylls/be.wav" },
            { id: 3, name: "ba", sound: "/assets/sylls/ba.wav" },
            { id: 4, name: "bu", sound: "/assets/sylls/bu.wav" },
            { id: 5, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 6, name: "rre", sound: "/assets/sylls/rre.wav" },
            { id: 7, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 8, name: "rren", sound: "/assets/sylls/rren.wav" },
            { id: 9, name: "kan", sound: "/assets/sylls/kan.wav" },
            { id: 10, name: "na", sound: "/assets/sylls/na.wav" },
            { id: 11, name: "nga", sound: "/assets/sylls/nga.wav" }
        ]
    },
    {
        id: 147,
        name: "MIXPRE-173-20200415-EK-list-orig_233",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_233.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 1, name: "me", sound: "/assets/sylls/me.wav" },
            { id: 2, name: "ma", sound: "/assets/sylls/ma.wav" },
            { id: 3, name: "be", sound: "/assets/sylls/be.wav" },
            { id: 4, name: "bi", sound: "/assets/sylls/bi.wav" },
            { id: 5, name: "ke", sound: "/assets/sylls/ke.wav" },
            { id: 6, name: "men", sound: "/assets/sylls/men.wav" },
            { id: 7, name: "meng", sound: "/assets/sylls/meng.wav" },
            { id: 8, name: "ben", sound: "/assets/sylls/ben.wav" },
            { id: 9, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 10, name: "ku", sound: "/assets/sylls/ku.wav" },
            { id: 11, name: "bu", sound: "/assets/sylls/bu.wav" },
            { id: 12, name: "kuk", sound: "/assets/sylls/kuk.wav" },
            { id: 13, name: "kan", sound: "/assets/sylls/kan.wav" }
        ]
    },
    {
        id: 148,
        name: "MIXPRE-173-20200415-EK-list-orig_234",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_234.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 1, name: "ba", sound: "/assets/sylls/ba.wav" },
            { id: 2, name: "wa", sound: "/assets/sylls/wa.wav" },
            { id: 3, name: "be", sound: "/assets/sylls/be.wav" },
            { id: 4, name: "bi", sound: "/assets/sylls/bi.wav" },
            { id: 5, name: "me", sound: "/assets/sylls/me.wav" },
            { id: 6, name: "mi", sound: "/assets/sylls/mi.wav" },
            { id: 7, name: "ma", sound: "/assets/sylls/ma.wav" },
            { id: 8, name: "ben", sound: "/assets/sylls/ben.wav" },
            { id: 9, name: "men", sound: "/assets/sylls/men.wav" },
            { id: 10, name: "bu", sound: "/assets/sylls/bu.wav" },
            { id: 11, name: "ke", sound: "/assets/sylls/ke.wav" },
            { id: 12, name: "bun", sound: "/assets/sylls/bun.wav" },
            { id: 13, name: "kan", sound: "/assets/sylls/kan.wav" }
        ]
    },
    {
        id: 149,
        name: "MIXPRE-173-20200415-EK-list-orig_235",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_235.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 1, name: "yi", sound: "/assets/sylls/yi.wav" },
            { id: 2, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 3, name: "bu", sound: "/assets/sylls/bu.wav" },
            { id: 4, name: "be", sound: "/assets/sylls/be.wav" },
            { id: 5, name: "bi", sound: "/assets/sylls/bi.wav" },
            { id: 6, name: "bo", sound: "/assets/sylls/bo.wav" },
            { id: 7, name: "bun", sound: "/assets/sylls/bun.wav" }
        ]
    },
    {
        id: 150,
        name: "MIXPRE-173-20200415-EK-list-orig_236",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_236.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 1, name: "rre", sound: "/assets/sylls/rre.wav" },
            { id: 2, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 3, name: "bi", sound: "/assets/sylls/bi.wav" },
            { id: 4, name: "be", sound: "/assets/sylls/be.wav" },
            { id: 5, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 6, name: "ku", sound: "/assets/sylls/ku.wav" },
            { id: 7, name: "bu", sound: "/assets/sylls/bu.wav" },
            { id: 8, name: "kun", sound: "/assets/sylls/kun.wav" },
            { id: 9, name: "bun", sound: "/assets/sylls/bun.wav" },
            { id: 10, name: "ne", sound: "/assets/sylls/ne.wav" },
            { id: 11, name: "na", sound: "/assets/sylls/na.wav" }
        ]
    },
    {
        id: 151,
        name: "MIXPRE-173-20200415-EK-list-orig_24",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_24.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 1, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 2, name: "karr", sound: "/assets/sylls/karr.wav" },
            { id: 3, name: "rre", sound: "/assets/sylls/rre.wav" },
            { id: 4, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 5, name: "bi", sound: "/assets/sylls/bi.wav" }
        ]
    },
    {
        id: 152,
        name: "MIXPRE-173-20200415-EK-list-orig_25",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_25.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 1, name: "wa", sound: "/assets/sylls/wa.wav" },
            { id: 2, name: "kan", sound: "/assets/sylls/kan.wav" },
            { id: 3, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 4, name: "bi", sound: "/assets/sylls/bi.wav" }
        ]
    },
    {
        id: 153,
        name: "MIXPRE-173-20200415-EK-list-orig_26",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_26.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 1, name: "ke", sound: "/assets/sylls/ke.wav" },
            { id: 2, name: "karr", sound: "/assets/sylls/karr.wav" },
            { id: 3, name: "kan", sound: "/assets/sylls/kan.wav" },
            { id: 4, name: "rre", sound: "/assets/sylls/rre.wav" },
            { id: 5, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 6, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 7, name: "di", sound: "/assets/sylls/di.wav" }
        ]
    },
    {
        id: 154,
        name: "MIXPRE-173-20200415-EK-list-orig_27",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_27.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 1, name: "karr", sound: "/assets/sylls/karr.wav" },
            { id: 2, name: "kan", sound: "/assets/sylls/kan.wav" },
            { id: 3, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 4, name: "rre", sound: "/assets/sylls/rre.wav" },
            { id: 5, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 6, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 7, name: "ne", sound: "/assets/sylls/ne.wav" },
            { id: 8, name: "me", sound: "/assets/sylls/me.wav" },
            { id: 9, name: "yi", sound: "/assets/sylls/yi.wav" }
        ]
    },
    {
        id: 155,
        name: "MIXPRE-173-20200415-EK-list-orig_28",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_28.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 1, name: "ke", sound: "/assets/sylls/ke.wav" },
            { id: 2, name: "kan", sound: "/assets/sylls/kan.wav" },
            { id: 3, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 4, name: "ni", sound: "/assets/sylls/ni.wav" }
        ]
    },
    {
        id: 156,
        name: "MIXPRE-173-20200415-EK-list-orig_29",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_29.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 1, name: "kan", sound: "/assets/sylls/kan.wav" },
            { id: 2, name: "nge", sound: "/assets/sylls/nge.wav" },
            { id: 3, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 4, name: "ke", sound: "/assets/sylls/ke.wav" }
        ]
    },
    {
        id: 157,
        name: "MIXPRE-173-20200415-EK-list-orig_3",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_3.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 1, name: "ke", sound: "/assets/sylls/ke.wav" },
            { id: 2, name: "bi", sound: "/assets/sylls/bi.wav" },
            { id: 3, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 4, name: "rre", sound: "/assets/sylls/rre.wav" },
            { id: 5, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 6, name: "kan", sound: "/assets/sylls/kan.wav" },
            { id: 7, name: "ni", sound: "/assets/sylls/ni.wav" }
        ]
    },
    {
        id: 158,
        name: "MIXPRE-173-20200415-EK-list-orig_30",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_30.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 1, name: "wa", sound: "/assets/sylls/wa.wav" },
            { id: 2, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 3, name: "ba", sound: "/assets/sylls/ba.wav" },
            { id: 4, name: "bi", sound: "/assets/sylls/bi.wav" },
            { id: 5, name: "be", sound: "/assets/sylls/be.wav" },
            { id: 6, name: "bu", sound: "/assets/sylls/bu.wav" },
            { id: 7, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 8, name: "bin", sound: "/assets/sylls/bin.wav" },
            { id: 9, name: "ben", sound: "/assets/sylls/ben.wav" },
            { id: 10, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 11, name: "rre", sound: "/assets/sylls/rre.wav" },
            { id: 12, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 13, name: "ke", sound: "/assets/sylls/ke.wav" },
            { id: 14, name: "bo", sound: "/assets/sylls/bo.wav" }
        ]
    },
    {
        id: 159,
        name: "MIXPRE-173-20200415-EK-list-orig_31",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_31.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 1, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 2, name: "wa", sound: "/assets/sylls/wa.wav" },
            { id: 3, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 4, name: "bi", sound: "/assets/sylls/bi.wav" },
            { id: 5, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 6, name: "bin", sound: "/assets/sylls/bin.wav" },
            { id: 7, name: "bu", sound: "/assets/sylls/bu.wav" },
            { id: 8, name: "bo", sound: "/assets/sylls/bo.wav" },
            { id: 9, name: "ku", sound: "/assets/sylls/ku.wav" },
            { id: 10, name: "bun", sound: "/assets/sylls/bun.wav" }
        ]
    },
    {
        id: 160,
        name: "MIXPRE-173-20200415-EK-list-orig_32",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_32.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 1, name: "be", sound: "/assets/sylls/be.wav" },
            { id: 2, name: "bi", sound: "/assets/sylls/bi.wav" },
            { id: 3, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 4, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 5, name: "rre", sound: "/assets/sylls/rre.wav" },
            { id: 6, name: "yi", sound: "/assets/sylls/yi.wav" },
            { id: 7, name: "ba", sound: "/assets/sylls/ba.wav" },
            { id: 8, name: "bu", sound: "/assets/sylls/bu.wav" },
            { id: 9, name: "bo", sound: "/assets/sylls/bo.wav" },
            { id: 10, name: "ku", sound: "/assets/sylls/ku.wav" }
        ]
    },
    {
        id: 161,
        name: "MIXPRE-173-20200415-EK-list-orig_33",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_33.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 1, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 2, name: "wa", sound: "/assets/sylls/wa.wav" },
            { id: 3, name: "rre", sound: "/assets/sylls/rre.wav" },
            { id: 4, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 5, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 6, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 7, name: "bi", sound: "/assets/sylls/bi.wav" },
            { id: 8, name: "be", sound: "/assets/sylls/be.wav" },
            { id: 9, name: "ben", sound: "/assets/sylls/ben.wav" },
            { id: 10, name: "ma", sound: "/assets/sylls/ma.wav" },
            { id: 11, name: "me", sound: "/assets/sylls/me.wav" },
            { id: 12, name: "na", sound: "/assets/sylls/na.wav" },
            { id: 13, name: "ngan", sound: "/assets/sylls/ngan.wav" }
        ]
    },
    {
        id: 162,
        name: "MIXPRE-173-20200415-EK-list-orig_34",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_34.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 1, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 2, name: "rre", sound: "/assets/sylls/rre.wav" },
            { id: 3, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 4, name: "bi", sound: "/assets/sylls/bi.wav" },
            { id: 5, name: "be", sound: "/assets/sylls/be.wav" },
            { id: 6, name: "bu", sound: "/assets/sylls/bu.wav" },
            { id: 7, name: "ba", sound: "/assets/sylls/ba.wav" },
            { id: 8, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 9, name: "bin", sound: "/assets/sylls/bin.wav" },
            { id: 10, name: "ben", sound: "/assets/sylls/ben.wav" },
            { id: 11, name: "bun", sound: "/assets/sylls/bun.wav" },
            { id: 12, name: "na", sound: "/assets/sylls/na.wav" },
            { id: 13, name: "ma", sound: "/assets/sylls/ma.wav" },
            { id: 14, name: "me", sound: "/assets/sylls/me.wav" },
            { id: 15, name: "man", sound: "/assets/sylls/man.wav" }
        ]
    },
    {
        id: 163,
        name: "MIXPRE-173-20200415-EK-list-orig_35",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_35.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "bi", sound: "/assets/sylls/bi.wav" },
            { id: 1, name: "be", sound: "/assets/sylls/be.wav" },
            { id: 2, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 3, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 4, name: "rre", sound: "/assets/sylls/rre.wav" },
            { id: 5, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 6, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 7, name: "ben", sound: "/assets/sylls/ben.wav" },
            { id: 8, name: "ma", sound: "/assets/sylls/ma.wav" },
            { id: 9, name: "me", sound: "/assets/sylls/me.wav" },
            { id: 10, name: "na", sound: "/assets/sylls/na.wav" },
            { id: 11, name: "nga", sound: "/assets/sylls/nga.wav" }
        ]
    },
    {
        id: 164,
        name: "MIXPRE-173-20200415-EK-list-orig_36",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_36.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "be", sound: "/assets/sylls/be.wav" },
            { id: 1, name: "ba", sound: "/assets/sylls/ba.wav" },
            { id: 2, name: "bi", sound: "/assets/sylls/bi.wav" },
            { id: 3, name: "bu", sound: "/assets/sylls/bu.wav" },
            { id: 4, name: "ben", sound: "/assets/sylls/ben.wav" },
            { id: 5, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 6, name: "rre", sound: "/assets/sylls/rre.wav" },
            { id: 7, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 8, name: "yi", sound: "/assets/sylls/yi.wav" },
            { id: 9, name: "ke", sound: "/assets/sylls/ke.wav" },
            { id: 10, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 11, name: "ku", sound: "/assets/sylls/ku.wav" },
            { id: 12, name: "ma", sound: "/assets/sylls/ma.wav" },
            { id: 13, name: "me", sound: "/assets/sylls/me.wav" },
            { id: 14, name: "mi", sound: "/assets/sylls/mi.wav" },
            { id: 15, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 16, name: "na", sound: "/assets/sylls/na.wav" }
        ]
    },
    {
        id: 165,
        name: "MIXPRE-173-20200415-EK-list-orig_37",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_37.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 1, name: "bi", sound: "/assets/sylls/bi.wav" },
            { id: 2, name: "ba", sound: "/assets/sylls/ba.wav" },
            { id: 3, name: "bu", sound: "/assets/sylls/bu.wav" },
            { id: 4, name: "be", sound: "/assets/sylls/be.wav" },
            { id: 5, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 6, name: "ben", sound: "/assets/sylls/ben.wav" },
            { id: 7, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 8, name: "rre", sound: "/assets/sylls/rre.wav" },
            { id: 9, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 10, name: "ma", sound: "/assets/sylls/ma.wav" },
            { id: 11, name: "me", sound: "/assets/sylls/me.wav" },
            { id: 12, name: "wa", sound: "/assets/sylls/wa.wav" },
            { id: 13, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 14, name: "men", sound: "/assets/sylls/men.wav" },
            { id: 15, name: "bo", sound: "/assets/sylls/bo.wav" },
            { id: 16, name: "ngu", sound: "/assets/sylls/ngu.wav" }
        ]
    },
    {
        id: 166,
        name: "MIXPRE-173-20200415-EK-list-orig_38",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_38.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ma", sound: "/assets/sylls/ma.wav" },
            { id: 1, name: "me", sound: "/assets/sylls/me.wav" },
            { id: 2, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 3, name: "man", sound: "/assets/sylls/man.wav" },
            { id: 4, name: "marn", sound: "/assets/sylls/marn.wav" },
            { id: 5, name: "men", sound: "/assets/sylls/men.wav" },
            { id: 6, name: "bu", sound: "/assets/sylls/bu.wav" },
            { id: 7, name: "bo", sound: "/assets/sylls/bo.wav" },
            { id: 8, name: "ba", sound: "/assets/sylls/ba.wav" }
        ]
    },
    {
        id: 167,
        name: "MIXPRE-173-20200415-EK-list-orig_39",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_39.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 1, name: "wa", sound: "/assets/sylls/wa.wav" },
            { id: 2, name: "bi", sound: "/assets/sylls/bi.wav" },
            { id: 3, name: "ba", sound: "/assets/sylls/ba.wav" },
            { id: 4, name: "be", sound: "/assets/sylls/be.wav" },
            { id: 5, name: "bu", sound: "/assets/sylls/bu.wav" },
            { id: 6, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 7, name: "bin", sound: "/assets/sylls/bin.wav" },
            { id: 8, name: "ben", sound: "/assets/sylls/ben.wav" },
            { id: 9, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 10, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 11, name: "ma", sound: "/assets/sylls/ma.wav" },
            { id: 12, name: "me", sound: "/assets/sylls/me.wav" },
            { id: 13, name: "man", sound: "/assets/sylls/man.wav" },
            { id: 14, name: "marn", sound: "/assets/sylls/marn.wav" },
            { id: 15, name: "men", sound: "/assets/sylls/men.wav" },
            { id: 16, name: "ngu", sound: "/assets/sylls/ngu.wav" },
            { id: 17, name: "ku", sound: "/assets/sylls/ku.wav" },
            { id: 18, name: "bun", sound: "/assets/sylls/bun.wav" }
        ]
    },
    {
        id: 168,
        name: "MIXPRE-173-20200415-EK-list-orig_4",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_4.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 1, name: "ba", sound: "/assets/sylls/ba.wav" },
            { id: 2, name: "karr", sound: "/assets/sylls/karr.wav" },
            { id: 3, name: "kan", sound: "/assets/sylls/kan.wav" },
            { id: 4, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 5, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 6, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 7, name: "bo", sound: "/assets/sylls/bo.wav" },
            { id: 8, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 9, name: "ngu", sound: "/assets/sylls/ngu.wav" },
            { id: 10, name: "wa", sound: "/assets/sylls/wa.wav" },
            { id: 11, name: "ma", sound: "/assets/sylls/ma.wav" },
            { id: 12, name: "ngan", sound: "/assets/sylls/ngan.wav" },
            { id: 13, name: "ngun", sound: "/assets/sylls/ngun.wav" }
        ]
    },
    {
        id: 169,
        name: "MIXPRE-173-20200415-EK-list-orig_40",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_40.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "bu", sound: "/assets/sylls/bu.wav" },
            { id: 1, name: "be", sound: "/assets/sylls/be.wav" },
            { id: 2, name: "bi", sound: "/assets/sylls/bi.wav" },
            { id: 3, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 4, name: "yi", sound: "/assets/sylls/yi.wav" },
            { id: 5, name: "ma", sound: "/assets/sylls/ma.wav" },
            { id: 6, name: "me", sound: "/assets/sylls/me.wav" },
            { id: 7, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 8, name: "man", sound: "/assets/sylls/man.wav" },
            { id: 9, name: "marn", sound: "/assets/sylls/marn.wav" },
            { id: 10, name: "men", sound: "/assets/sylls/men.wav" }
        ]
    },
    {
        id: 170,
        name: "MIXPRE-173-20200415-EK-list-orig_41",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_41.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 1, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 2, name: "wo", sound: "/assets/sylls/wo.wav" },
            { id: 3, name: "wa", sound: "/assets/sylls/wa.wav" },
            { id: 4, name: "bu", sound: "/assets/sylls/bu.wav" },
            { id: 5, name: "ka", sound: "/assets/sylls/ka.wav" }
        ]
    },
    {
        id: 171,
        name: "MIXPRE-173-20200415-EK-list-orig_42",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_42.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 1, name: "rre", sound: "/assets/sylls/rre.wav" },
            { id: 2, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 3, name: "wo", sound: "/assets/sylls/wo.wav" },
            { id: 4, name: "ngu", sound: "/assets/sylls/ngu.wav" },
            { id: 5, name: "bu", sound: "/assets/sylls/bu.wav" },
            { id: 6, name: "wa", sound: "/assets/sylls/wa.wav" },
            { id: 7, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 8, name: "ke", sound: "/assets/sylls/ke.wav" },
            { id: 9, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 10, name: "be", sound: "/assets/sylls/be.wav" },
            { id: 11, name: "bi", sound: "/assets/sylls/bi.wav" }
        ]
    },
    {
        id: 172,
        name: "MIXPRE-173-20200415-EK-list-orig_43",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_43.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 1, name: "ke", sound: "/assets/sylls/ke.wav" },
            { id: 2, name: "wa", sound: "/assets/sylls/wa.wav" },
            { id: 3, name: "ba", sound: "/assets/sylls/ba.wav" },
            { id: 4, name: "bi", sound: "/assets/sylls/bi.wav" },
            { id: 5, name: "bu", sound: "/assets/sylls/bu.wav" },
            { id: 6, name: "be", sound: "/assets/sylls/be.wav" },
            { id: 7, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 8, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 9, name: "bin", sound: "/assets/sylls/bin.wav" },
            { id: 10, name: "ben", sound: "/assets/sylls/ben.wav" },
            { id: 11, name: "ni", sound: "/assets/sylls/ni.wav" }
        ]
    },
    {
        id: 173,
        name: "MIXPRE-173-20200415-EK-list-orig_44",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_44.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 1, name: "ku", sound: "/assets/sylls/ku.wav" },
            { id: 2, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 3, name: "rre", sound: "/assets/sylls/rre.wav" },
            { id: 4, name: "ke", sound: "/assets/sylls/ke.wav" },
            { id: 5, name: "wa", sound: "/assets/sylls/wa.wav" },
            { id: 6, name: "kan", sound: "/assets/sylls/kan.wav" },
            { id: 7, name: "ben", sound: "/assets/sylls/ben.wav" },
            { id: 8, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 9, name: "ne", sound: "/assets/sylls/ne.wav" },
            { id: 10, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 11, name: "me", sound: "/assets/sylls/me.wav" },
            { id: 12, name: "ma", sound: "/assets/sylls/ma.wav" },
            { id: 13, name: "rne", sound: "/assets/sylls/rne.wav" }
        ]
    },
    {
        id: 174,
        name: "MIXPRE-173-20200415-EK-list-orig_45",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_45.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 1, name: "wa", sound: "/assets/sylls/wa.wav" },
            { id: 2, name: "bi", sound: "/assets/sylls/bi.wav" },
            { id: 3, name: "be", sound: "/assets/sylls/be.wav" },
            { id: 4, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 5, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 6, name: "rre", sound: "/assets/sylls/rre.wav" },
            { id: 7, name: "ku", sound: "/assets/sylls/ku.wav" },
            { id: 8, name: "wo", sound: "/assets/sylls/wo.wav" },
            { id: 9, name: "kan", sound: "/assets/sylls/kan.wav" },
            { id: 10, name: "ke", sound: "/assets/sylls/ke.wav" },
            { id: 11, name: "ne", sound: "/assets/sylls/ne.wav" },
            { id: 12, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 13, name: "me", sound: "/assets/sylls/me.wav" },
            { id: 14, name: "ma", sound: "/assets/sylls/ma.wav" },
            { id: 15, name: "rne", sound: "/assets/sylls/rne.wav" }
        ]
    },
    {
        id: 175,
        name: "MIXPRE-173-20200415-EK-list-orig_46",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_46.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 1, name: "wa", sound: "/assets/sylls/wa.wav" },
            { id: 2, name: "we", sound: "/assets/sylls/we.wav" },
            { id: 3, name: "wo", sound: "/assets/sylls/wo.wav" },
            { id: 4, name: "kan", sound: "/assets/sylls/kan.wav" },
            { id: 5, name: "me", sound: "/assets/sylls/me.wav" },
            { id: 6, name: "ma", sound: "/assets/sylls/ma.wav" },
            { id: 7, name: "ne", sound: "/assets/sylls/ne.wav" },
            { id: 8, name: "rne", sound: "/assets/sylls/rne.wav" }
        ]
    },
    {
        id: 176,
        name: "MIXPRE-173-20200415-EK-list-orig_47",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_47.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 1, name: "wa", sound: "/assets/sylls/wa.wav" },
            { id: 2, name: "wo", sound: "/assets/sylls/wo.wav" },
            { id: 3, name: "rre", sound: "/assets/sylls/rre.wav" },
            { id: 4, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 5, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 6, name: "rne", sound: "/assets/sylls/rne.wav" },
            { id: 7, name: "me", sound: "/assets/sylls/me.wav" },
            { id: 8, name: "mi", sound: "/assets/sylls/mi.wav" },
            { id: 9, name: "ma", sound: "/assets/sylls/ma.wav" }
        ]
    },
    {
        id: 177,
        name: "MIXPRE-173-20200415-EK-list-orig_48",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_48.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ke", sound: "/assets/sylls/ke.wav" },
            { id: 1, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 2, name: "bi", sound: "/assets/sylls/bi.wav" },
            { id: 3, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 4, name: "kan", sound: "/assets/sylls/kan.wav" },
            { id: 5, name: "ben", sound: "/assets/sylls/ben.wav" },
            { id: 6, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 7, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 8, name: "ma", sound: "/assets/sylls/ma.wav" },
            { id: 9, name: "me", sound: "/assets/sylls/me.wav" },
            { id: 10, name: "man", sound: "/assets/sylls/man.wav" },
            { id: 11, name: "men", sound: "/assets/sylls/men.wav" },
            { id: 12, name: "bu", sound: "/assets/sylls/bu.wav" },
            { id: 13, name: "ba", sound: "/assets/sylls/ba.wav" },
            { id: 14, name: "ku", sound: "/assets/sylls/ku.wav" },
            { id: 15, name: "ngu", sound: "/assets/sylls/ngu.wav" },
            { id: 16, name: "bun", sound: "/assets/sylls/bun.wav" },
            { id: 17, name: "kun", sound: "/assets/sylls/kun.wav" },
            { id: 18, name: "ngun", sound: "/assets/sylls/ngun.wav" },
            { id: 19, name: "ne", sound: "/assets/sylls/ne.wav" },
            { id: 20, name: "na", sound: "/assets/sylls/na.wav" },
            { id: 21, name: "rne", sound: "/assets/sylls/rne.wav" }
        ]
    },
    {
        id: 178,
        name: "MIXPRE-173-20200415-EK-list-orig_49",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_49.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "bi", sound: "/assets/sylls/bi.wav" },
            { id: 1, name: "be", sound: "/assets/sylls/be.wav" },
            { id: 2, name: "bu", sound: "/assets/sylls/bu.wav" },
            { id: 3, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 4, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 5, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 6, name: "rre", sound: "/assets/sylls/rre.wav" },
            { id: 7, name: "ma", sound: "/assets/sylls/ma.wav" },
            { id: 8, name: "me", sound: "/assets/sylls/me.wav" },
            { id: 9, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 10, name: "man", sound: "/assets/sylls/man.wav" },
            { id: 11, name: "marn", sound: "/assets/sylls/marn.wav" },
            { id: 12, name: "men", sound: "/assets/sylls/men.wav" },
            { id: 13, name: "bun", sound: "/assets/sylls/bun.wav" },
            { id: 14, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 15, name: "ne", sound: "/assets/sylls/ne.wav" },
            { id: 16, name: "na", sound: "/assets/sylls/na.wav" }
        ]
    },
    {
        id: 179,
        name: "MIXPRE-173-20200415-EK-list-orig_5",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_5.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "wa", sound: "/assets/sylls/wa.wav" },
            { id: 1, name: "ba", sound: "/assets/sylls/ba.wav" },
            { id: 2, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 3, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 4, name: "kan", sound: "/assets/sylls/kan.wav" },
            { id: 5, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 6, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 7, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 8, name: "wo", sound: "/assets/sylls/wo.wav" },
            { id: 9, name: "ngu", sound: "/assets/sylls/ngu.wav" },
            { id: 10, name: "ma", sound: "/assets/sylls/ma.wav" },
            { id: 11, name: "ngan", sound: "/assets/sylls/ngan.wav" },
            { id: 12, name: "ngun", sound: "/assets/sylls/ngun.wav" },
            { id: 13, name: "ne", sound: "/assets/sylls/ne.wav" },
            { id: 14, name: "na", sound: "/assets/sylls/na.wav" },
            { id: 15, name: "mi", sound: "/assets/sylls/mi.wav" }
        ]
    },
    {
        id: 180,
        name: "MIXPRE-173-20200415-EK-list-orig_50",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_50.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 1, name: "bi", sound: "/assets/sylls/bi.wav" },
            { id: 2, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 3, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 4, name: "rre", sound: "/assets/sylls/rre.wav" },
            { id: 5, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 6, name: "ma", sound: "/assets/sylls/ma.wav" },
            { id: 7, name: "me", sound: "/assets/sylls/me.wav" },
            { id: 8, name: "man", sound: "/assets/sylls/man.wav" },
            { id: 9, name: "marn", sound: "/assets/sylls/marn.wav" },
            { id: 10, name: "men", sound: "/assets/sylls/men.wav" },
            { id: 11, name: "bu", sound: "/assets/sylls/bu.wav" },
            { id: 12, name: "ba", sound: "/assets/sylls/ba.wav" },
            { id: 13, name: "bun", sound: "/assets/sylls/bun.wav" },
            { id: 14, name: "ne", sound: "/assets/sylls/ne.wav" },
            { id: 15, name: "yi", sound: "/assets/sylls/yi.wav" }
        ]
    },
    {
        id: 181,
        name: "MIXPRE-173-20200415-EK-list-orig_51",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_51.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 1, name: "rre", sound: "/assets/sylls/rre.wav" },
            { id: 2, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 3, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 4, name: "ba", sound: "/assets/sylls/ba.wav" },
            { id: 5, name: "wa", sound: "/assets/sylls/wa.wav" },
            { id: 6, name: "ma", sound: "/assets/sylls/ma.wav" },
            { id: 7, name: "me", sound: "/assets/sylls/me.wav" },
            { id: 8, name: "man", sound: "/assets/sylls/man.wav" },
            { id: 9, name: "marn", sound: "/assets/sylls/marn.wav" },
            { id: 10, name: "men", sound: "/assets/sylls/men.wav" },
            { id: 11, name: "bu", sound: "/assets/sylls/bu.wav" },
            { id: 12, name: "ku", sound: "/assets/sylls/ku.wav" },
            { id: 13, name: "bun", sound: "/assets/sylls/bun.wav" },
            { id: 14, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 15, name: "mi", sound: "/assets/sylls/mi.wav" }
        ]
    },
    {
        id: 182,
        name: "MIXPRE-173-20200415-EK-list-orig_52",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_52.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "bi", sound: "/assets/sylls/bi.wav" },
            { id: 1, name: "be", sound: "/assets/sylls/be.wav" },
            { id: 2, name: "ben", sound: "/assets/sylls/ben.wav" },
            { id: 3, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 4, name: "rre", sound: "/assets/sylls/rre.wav" },
            { id: 5, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 6, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 7, name: "bo", sound: "/assets/sylls/bo.wav" },
            { id: 8, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 9, name: "wo", sound: "/assets/sylls/wo.wav" },
            { id: 10, name: "ma", sound: "/assets/sylls/ma.wav" },
            { id: 11, name: "me", sound: "/assets/sylls/me.wav" },
            { id: 12, name: "man", sound: "/assets/sylls/man.wav" },
            { id: 13, name: "marn", sound: "/assets/sylls/marn.wav" },
            { id: 14, name: "men", sound: "/assets/sylls/men.wav" },
            { id: 15, name: "bu", sound: "/assets/sylls/bu.wav" },
            { id: 16, name: "bun", sound: "/assets/sylls/bun.wav" },
            { id: 17, name: "ne", sound: "/assets/sylls/ne.wav" },
            { id: 18, name: "na", sound: "/assets/sylls/na.wav" },
            { id: 19, name: "rne", sound: "/assets/sylls/rne.wav" }
        ]
    },
    {
        id: 183,
        name: "MIXPRE-173-20200415-EK-list-orig_53",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_53.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "be", sound: "/assets/sylls/be.wav" },
            { id: 1, name: "bi", sound: "/assets/sylls/bi.wav" },
            { id: 2, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 3, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 4, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 5, name: "rre", sound: "/assets/sylls/rre.wav" },
            { id: 6, name: "bu", sound: "/assets/sylls/bu.wav" },
            { id: 7, name: "bo", sound: "/assets/sylls/bo.wav" },
            { id: 8, name: "ku", sound: "/assets/sylls/ku.wav" },
            { id: 9, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 10, name: "ma", sound: "/assets/sylls/ma.wav" },
            { id: 11, name: "me", sound: "/assets/sylls/me.wav" },
            { id: 12, name: "wa", sound: "/assets/sylls/wa.wav" },
            { id: 13, name: "ngan", sound: "/assets/sylls/ngan.wav" },
            { id: 14, name: "man", sound: "/assets/sylls/man.wav" },
            { id: 15, name: "men", sound: "/assets/sylls/men.wav" },
            { id: 16, name: "kan", sound: "/assets/sylls/kan.wav" }
        ]
    },
    {
        id: 184,
        name: "MIXPRE-173-20200415-EK-list-orig_54",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_54.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 1, name: "ke", sound: "/assets/sylls/ke.wav" },
            { id: 2, name: "ku", sound: "/assets/sylls/ku.wav" },
            { id: 3, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 4, name: "bi", sound: "/assets/sylls/bi.wav" },
            { id: 5, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 6, name: "rre", sound: "/assets/sylls/rre.wav" },
            { id: 7, name: "bo", sound: "/assets/sylls/bo.wav" },
            { id: 8, name: "ba", sound: "/assets/sylls/ba.wav" },
            { id: 9, name: "bu", sound: "/assets/sylls/bu.wav" },
            { id: 10, name: "bom", sound: "/assets/sylls/bom.wav" },
            { id: 11, name: "ma", sound: "/assets/sylls/ma.wav" },
            { id: 12, name: "man", sound: "/assets/sylls/man.wav" },
            { id: 13, name: "marn", sound: "/assets/sylls/marn.wav" },
            { id: 14, name: "bun", sound: "/assets/sylls/bun.wav" },
            { id: 15, name: "ne", sound: "/assets/sylls/ne.wav" },
            { id: 16, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 17, name: "na", sound: "/assets/sylls/na.wav" },
            { id: 18, name: "rne", sound: "/assets/sylls/rne.wav" },
            { id: 19, name: "me", sound: "/assets/sylls/me.wav" }
        ]
    },
    {
        id: 185,
        name: "MIXPRE-173-20200415-EK-list-orig_55",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_55.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 1, name: "wa", sound: "/assets/sylls/wa.wav" },
            { id: 2, name: "ma", sound: "/assets/sylls/ma.wav" },
            { id: 3, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 4, name: "rre", sound: "/assets/sylls/rre.wav" },
            { id: 5, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 6, name: "me", sound: "/assets/sylls/me.wav" },
            { id: 7, name: "ba", sound: "/assets/sylls/ba.wav" },
            { id: 8, name: "man", sound: "/assets/sylls/man.wav" },
            { id: 9, name: "men", sound: "/assets/sylls/men.wav" },
            { id: 10, name: "bu", sound: "/assets/sylls/bu.wav" },
            { id: 11, name: "bun", sound: "/assets/sylls/bun.wav" }
        ]
    },
    {
        id: 186,
        name: "MIXPRE-173-20200415-EK-list-orig_56",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_56.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 1, name: "rre", sound: "/assets/sylls/rre.wav" },
            { id: 2, name: "ma", sound: "/assets/sylls/ma.wav" },
            { id: 3, name: "me", sound: "/assets/sylls/me.wav" },
            { id: 4, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 5, name: "man", sound: "/assets/sylls/man.wav" },
            { id: 6, name: "marn", sound: "/assets/sylls/marn.wav" },
            { id: 7, name: "men", sound: "/assets/sylls/men.wav" },
            { id: 8, name: "bu", sound: "/assets/sylls/bu.wav" },
            { id: 9, name: "bo", sound: "/assets/sylls/bo.wav" },
            { id: 10, name: "ba", sound: "/assets/sylls/ba.wav" },
            { id: 11, name: "bun", sound: "/assets/sylls/bun.wav" }
        ]
    },
    {
        id: 187,
        name: "MIXPRE-173-20200415-EK-list-orig_57",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_57.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 1, name: "rre", sound: "/assets/sylls/rre.wav" },
            { id: 2, name: "yi", sound: "/assets/sylls/yi.wav" },
            { id: 3, name: "ma", sound: "/assets/sylls/ma.wav" },
            { id: 4, name: "me", sound: "/assets/sylls/me.wav" },
            { id: 5, name: "man", sound: "/assets/sylls/man.wav" },
            { id: 6, name: "marn", sound: "/assets/sylls/marn.wav" },
            { id: 7, name: "men", sound: "/assets/sylls/men.wav" },
            { id: 8, name: "bu", sound: "/assets/sylls/bu.wav" },
            { id: 9, name: "ba", sound: "/assets/sylls/ba.wav" }
        ]
    },
    {
        id: 188,
        name: "MIXPRE-173-20200415-EK-list-orig_58",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_58.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "bu", sound: "/assets/sylls/bu.wav" },
            { id: 1, name: "be", sound: "/assets/sylls/be.wav" },
            { id: 2, name: "bi", sound: "/assets/sylls/bi.wav" },
            { id: 3, name: "ku", sound: "/assets/sylls/ku.wav" },
            { id: 4, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 5, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 6, name: "mi", sound: "/assets/sylls/mi.wav" },
            { id: 7, name: "bo", sound: "/assets/sylls/bo.wav" },
            { id: 8, name: "ba", sound: "/assets/sylls/ba.wav" }
        ]
    },
    {
        id: 189,
        name: "MIXPRE-173-20200415-EK-list-orig_59",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_59.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ku", sound: "/assets/sylls/ku.wav" },
            { id: 1, name: "bu", sound: "/assets/sylls/bu.wav" },
            { id: 2, name: "kun", sound: "/assets/sylls/kun.wav" },
            { id: 3, name: "bun", sound: "/assets/sylls/bun.wav" },
            { id: 4, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 5, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 6, name: "bo", sound: "/assets/sylls/bo.wav" },
            { id: 7, name: "ba", sound: "/assets/sylls/ba.wav" },
            { id: 8, name: "ka", sound: "/assets/sylls/ka.wav" }
        ]
    },
    {
        id: 190,
        name: "MIXPRE-173-20200415-EK-list-orig_6",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_6.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ba", sound: "/assets/sylls/ba.wav" },
            { id: 1, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 2, name: "kan", sound: "/assets/sylls/kan.wav" },
            { id: 3, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 4, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 5, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 6, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 7, name: "ngu", sound: "/assets/sylls/ngu.wav" },
            { id: 8, name: "ma", sound: "/assets/sylls/ma.wav" },
            { id: 9, name: "ngan", sound: "/assets/sylls/ngan.wav" },
            { id: 10, name: "ngun", sound: "/assets/sylls/ngun.wav" },
            { id: 11, name: "ne", sound: "/assets/sylls/ne.wav" },
            { id: 12, name: "na", sound: "/assets/sylls/na.wav" }
        ]
    },
    {
        id: 191,
        name: "MIXPRE-173-20200415-EK-list-orig_60",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_60.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "bi", sound: "/assets/sylls/bi.wav" },
            { id: 1, name: "bu", sound: "/assets/sylls/bu.wav" },
            { id: 2, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 3, name: "rre", sound: "/assets/sylls/rre.wav" },
            { id: 4, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 5, name: "ma", sound: "/assets/sylls/ma.wav" },
            { id: 6, name: "me", sound: "/assets/sylls/me.wav" },
            { id: 7, name: "mi", sound: "/assets/sylls/mi.wav" },
            { id: 8, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 9, name: "wa", sound: "/assets/sylls/wa.wav" },
            { id: 10, name: "men", sound: "/assets/sylls/men.wav" },
            { id: 11, name: "bo", sound: "/assets/sylls/bo.wav" },
            { id: 12, name: "ba", sound: "/assets/sylls/ba.wav" }
        ]
    },
    {
        id: 192,
        name: "MIXPRE-173-20200415-EK-list-orig_61",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_61.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "bi", sound: "/assets/sylls/bi.wav" },
            { id: 1, name: "be", sound: "/assets/sylls/be.wav" },
            { id: 2, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 3, name: "bin", sound: "/assets/sylls/bin.wav" },
            { id: 4, name: "ben", sound: "/assets/sylls/ben.wav" },
            { id: 5, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 6, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 7, name: "ma", sound: "/assets/sylls/ma.wav" },
            { id: 8, name: "man", sound: "/assets/sylls/man.wav" },
            { id: 9, name: "marn", sound: "/assets/sylls/marn.wav" }
        ]
    },
    {
        id: 193,
        name: "MIXPRE-173-20200415-EK-list-orig_62",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_62.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 1, name: "ba", sound: "/assets/sylls/ba.wav" },
            { id: 2, name: "wa", sound: "/assets/sylls/wa.wav" },
            { id: 3, name: "karr", sound: "/assets/sylls/karr.wav" },
            { id: 4, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 5, name: "rre", sound: "/assets/sylls/rre.wav" },
            { id: 6, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 7, name: "ku", sound: "/assets/sylls/ku.wav" },
            { id: 8, name: "bu", sound: "/assets/sylls/bu.wav" },
            { id: 9, name: "kun", sound: "/assets/sylls/kun.wav" },
            { id: 10, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 11, name: "me", sound: "/assets/sylls/me.wav" },
            { id: 12, name: "ma", sound: "/assets/sylls/ma.wav" },
            { id: 13, name: "ne", sound: "/assets/sylls/ne.wav" },
            { id: 14, name: "meng", sound: "/assets/sylls/meng.wav" }
        ]
    },
    {
        id: 194,
        name: "MIXPRE-173-20200415-EK-list-orig_63",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_63.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 1, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 2, name: "kan", sound: "/assets/sylls/kan.wav" },
            { id: 3, name: "ba", sound: "/assets/sylls/ba.wav" },
            { id: 4, name: "be", sound: "/assets/sylls/be.wav" },
            { id: 5, name: "wa", sound: "/assets/sylls/wa.wav" },
            { id: 6, name: "na", sound: "/assets/sylls/na.wav" },
            { id: 7, name: "ma", sound: "/assets/sylls/ma.wav" },
            { id: 8, name: "ben", sound: "/assets/sylls/ben.wav" },
            { id: 9, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 10, name: "rre", sound: "/assets/sylls/rre.wav" },
            { id: 11, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 12, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 13, name: "bu", sound: "/assets/sylls/bu.wav" },
            { id: 14, name: "ku", sound: "/assets/sylls/ku.wav" },
            { id: 15, name: "me", sound: "/assets/sylls/me.wav" },
            { id: 16, name: "mi", sound: "/assets/sylls/mi.wav" }
        ]
    },
    {
        id: 195,
        name: "MIXPRE-173-20200415-EK-list-orig_64",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_64.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 1, name: "ba", sound: "/assets/sylls/ba.wav" },
            { id: 2, name: "be", sound: "/assets/sylls/be.wav" },
            { id: 3, name: "wa", sound: "/assets/sylls/wa.wav" },
            { id: 4, name: "ben", sound: "/assets/sylls/ben.wav" },
            { id: 5, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 6, name: "rre", sound: "/assets/sylls/rre.wav" },
            { id: 7, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 8, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 9, name: "ngu", sound: "/assets/sylls/ngu.wav" },
            { id: 10, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 11, name: "ma", sound: "/assets/sylls/ma.wav" },
            { id: 12, name: "bu", sound: "/assets/sylls/bu.wav" },
            { id: 13, name: "me", sound: "/assets/sylls/me.wav" }
        ]
    },
    {
        id: 196,
        name: "MIXPRE-173-20200415-EK-list-orig_65",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_65.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 1, name: "ke", sound: "/assets/sylls/ke.wav" },
            { id: 2, name: "wa", sound: "/assets/sylls/wa.wav" },
            { id: 3, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 4, name: "kan", sound: "/assets/sylls/kan.wav" },
            { id: 5, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 6, name: "rre", sound: "/assets/sylls/rre.wav" },
            { id: 7, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 8, name: "bu", sound: "/assets/sylls/bu.wav" }
        ]
    },
    {
        id: 197,
        name: "MIXPRE-173-20200415-EK-list-orig_66",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_66.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ke", sound: "/assets/sylls/ke.wav" },
            { id: 1, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 2, name: "rre", sound: "/assets/sylls/rre.wav" },
            { id: 3, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 4, name: "ba", sound: "/assets/sylls/ba.wav" },
            { id: 5, name: "be", sound: "/assets/sylls/be.wav" },
            { id: 6, name: "bu", sound: "/assets/sylls/bu.wav" },
            { id: 7, name: "ben", sound: "/assets/sylls/ben.wav" },
            { id: 8, name: "me", sound: "/assets/sylls/me.wav" },
            { id: 9, name: "ma", sound: "/assets/sylls/ma.wav" },
            { id: 10, name: "mi", sound: "/assets/sylls/mi.wav" },
            { id: 11, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 12, name: "kan", sound: "/assets/sylls/kan.wav" },
            { id: 13, name: "men", sound: "/assets/sylls/men.wav" },
            { id: 14, name: "meng", sound: "/assets/sylls/meng.wav" },
            { id: 15, name: "ngu", sound: "/assets/sylls/ngu.wav" },
            { id: 16, name: "ku", sound: "/assets/sylls/ku.wav" }
        ]
    },
    {
        id: 198,
        name: "MIXPRE-173-20200415-EK-list-orig_67",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_67.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 1, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 2, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 3, name: "na", sound: "/assets/sylls/na.wav" },
            { id: 4, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 5, name: "ma", sound: "/assets/sylls/ma.wav" },
            { id: 6, name: "me", sound: "/assets/sylls/me.wav" },
            { id: 7, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 8, name: "kan", sound: "/assets/sylls/kan.wav" }
        ]
    },
    {
        id: 199,
        name: "MIXPRE-173-20200415-EK-list-orig_68",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_68.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 1, name: "rre", sound: "/assets/sylls/rre.wav" },
            { id: 2, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 3, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 4, name: "rrinj", sound: "/assets/sylls/rrinj.wav" },
            { id: 5, name: "rren", sound: "/assets/sylls/rren.wav" },
            { id: 6, name: "na", sound: "/assets/sylls/na.wav" },
            { id: 7, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 8, name: "ke", sound: "/assets/sylls/ke.wav" },
            { id: 9, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 10, name: "ma", sound: "/assets/sylls/ma.wav" },
            { id: 11, name: "me", sound: "/assets/sylls/me.wav" }
        ]
    },
    {
        id: 200,
        name: "MIXPRE-173-20200415-EK-list-orig_69",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_69.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "bi", sound: "/assets/sylls/bi.wav" },
            { id: 1, name: "be", sound: "/assets/sylls/be.wav" },
            { id: 2, name: "bu", sound: "/assets/sylls/bu.wav" },
            { id: 3, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 4, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 5, name: "rre", sound: "/assets/sylls/rre.wav" },
            { id: 6, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 7, name: "me", sound: "/assets/sylls/me.wav" },
            { id: 8, name: "ma", sound: "/assets/sylls/ma.wav" },
            { id: 9, name: "mi", sound: "/assets/sylls/mi.wav" },
            { id: 10, name: "rne", sound: "/assets/sylls/rne.wav" }
        ]
    },
    {
        id: 201,
        name: "MIXPRE-173-20200415-EK-list-orig_7",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_7.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ma", sound: "/assets/sylls/ma.wav" },
            { id: 1, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 2, name: "na", sound: "/assets/sylls/na.wav" },
            { id: 3, name: "wa", sound: "/assets/sylls/wa.wav" },
            { id: 4, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 5, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 6, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 7, name: "ba", sound: "/assets/sylls/ba.wav" },
            { id: 8, name: "be", sound: "/assets/sylls/be.wav" },
            { id: 9, name: "bi", sound: "/assets/sylls/bi.wav" },
            { id: 10, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 11, name: "ben", sound: "/assets/sylls/ben.wav" },
            { id: 12, name: "kan", sound: "/assets/sylls/kan.wav" },
            { id: 13, name: "ke", sound: "/assets/sylls/ke.wav" }
        ]
    },
    {
        id: 202,
        name: "MIXPRE-173-20200415-EK-list-orig_70",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_70.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 1, name: "rre", sound: "/assets/sylls/rre.wav" },
            { id: 2, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 3, name: "me", sound: "/assets/sylls/me.wav" },
            { id: 4, name: "mi", sound: "/assets/sylls/mi.wav" },
            { id: 5, name: "ma", sound: "/assets/sylls/ma.wav" },
            { id: 6, name: "ne", sound: "/assets/sylls/ne.wav" }
        ]
    },
    {
        id: 203,
        name: "MIXPRE-173-20200415-EK-list-orig_71",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_71.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 1, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 2, name: "me", sound: "/assets/sylls/me.wav" },
            { id: 3, name: "ma", sound: "/assets/sylls/ma.wav" },
            { id: 4, name: "mi", sound: "/assets/sylls/mi.wav" }
        ]
    },
    {
        id: 204,
        name: "MIXPRE-173-20200415-EK-list-orig_72",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_72.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 1, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 2, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 3, name: "rre", sound: "/assets/sylls/rre.wav" },
            { id: 4, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 5, name: "yi", sound: "/assets/sylls/yi.wav" },
            { id: 6, name: "rren", sound: "/assets/sylls/rren.wav" },
            { id: 7, name: "me", sound: "/assets/sylls/me.wav" },
            { id: 8, name: "mi", sound: "/assets/sylls/mi.wav" },
            { id: 9, name: "ma", sound: "/assets/sylls/ma.wav" },
            { id: 10, name: "ne", sound: "/assets/sylls/ne.wav" },
            { id: 11, name: "rne", sound: "/assets/sylls/rne.wav" }
        ]
    },
    {
        id: 205,
        name: "MIXPRE-173-20200415-EK-list-orig_73",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_73.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 1, name: "ma", sound: "/assets/sylls/ma.wav" },
            { id: 2, name: "me", sound: "/assets/sylls/me.wav" },
            { id: 3, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 4, name: "na", sound: "/assets/sylls/na.wav" },
            { id: 5, name: "ngan", sound: "/assets/sylls/ngan.wav" },
            { id: 6, name: "man", sound: "/assets/sylls/man.wav" },
            { id: 7, name: "men", sound: "/assets/sylls/men.wav" },
            { id: 8, name: "kan", sound: "/assets/sylls/kan.wav" },
            { id: 9, name: "wo", sound: "/assets/sylls/wo.wav" },
            { id: 10, name: "bu", sound: "/assets/sylls/bu.wav" }
        ]
    },
    {
        id: 206,
        name: "MIXPRE-173-20200415-EK-list-orig_74",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_74.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ma", sound: "/assets/sylls/ma.wav" },
            { id: 1, name: "me", sound: "/assets/sylls/me.wav" },
            { id: 2, name: "na", sound: "/assets/sylls/na.wav" },
            { id: 3, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 4, name: "man", sound: "/assets/sylls/man.wav" },
            { id: 5, name: "marn", sound: "/assets/sylls/marn.wav" },
            { id: 6, name: "men", sound: "/assets/sylls/men.wav" },
            { id: 7, name: "ngan", sound: "/assets/sylls/ngan.wav" },
            { id: 8, name: "bo", sound: "/assets/sylls/bo.wav" },
            { id: 9, name: "bu", sound: "/assets/sylls/bu.wav" },
            { id: 10, name: "ba", sound: "/assets/sylls/ba.wav" }
        ]
    },
    {
        id: 207,
        name: "MIXPRE-173-20200415-EK-list-orig_75",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_75.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 1, name: "wa", sound: "/assets/sylls/wa.wav" },
            { id: 2, name: "ba", sound: "/assets/sylls/ba.wav" },
            { id: 3, name: "karr", sound: "/assets/sylls/karr.wav" },
            { id: 4, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 5, name: "rre", sound: "/assets/sylls/rre.wav" },
            { id: 6, name: "yi", sound: "/assets/sylls/yi.wav" },
            { id: 7, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 8, name: "bo", sound: "/assets/sylls/bo.wav" },
            { id: 9, name: "bu", sound: "/assets/sylls/bu.wav" },
            { id: 10, name: "bun", sound: "/assets/sylls/bun.wav" }
        ]
    },
    {
        id: 208,
        name: "MIXPRE-173-20200415-EK-list-orig_76",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_76.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 1, name: "rre", sound: "/assets/sylls/rre.wav" },
            { id: 2, name: "bo", sound: "/assets/sylls/bo.wav" },
            { id: 3, name: "ba", sound: "/assets/sylls/ba.wav" },
            { id: 4, name: "bu", sound: "/assets/sylls/bu.wav" },
            { id: 5, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 6, name: "wo", sound: "/assets/sylls/wo.wav" },
            { id: 7, name: "mi", sound: "/assets/sylls/mi.wav" },
            { id: 8, name: "me", sound: "/assets/sylls/me.wav" },
            { id: 9, name: "ma", sound: "/assets/sylls/ma.wav" },
            { id: 10, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 11, name: "ni", sound: "/assets/sylls/ni.wav" }
        ]
    },
    {
        id: 209,
        name: "MIXPRE-173-20200415-EK-list-orig_77",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_77.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 1, name: "wa", sound: "/assets/sylls/wa.wav" },
            { id: 2, name: "yi", sound: "/assets/sylls/yi.wav" },
            { id: 3, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 4, name: "bi", sound: "/assets/sylls/bi.wav" },
            { id: 5, name: "kan", sound: "/assets/sylls/kan.wav" },
            { id: 6, name: "me", sound: "/assets/sylls/me.wav" },
            { id: 7, name: "ma", sound: "/assets/sylls/ma.wav" },
            { id: 8, name: "ne", sound: "/assets/sylls/ne.wav" },
            { id: 9, name: "rne", sound: "/assets/sylls/rne.wav" }
        ]
    },
    {
        id: 210,
        name: "MIXPRE-173-20200415-EK-list-orig_78",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_78.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 1, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 2, name: "kan", sound: "/assets/sylls/kan.wav" },
            { id: 3, name: "me", sound: "/assets/sylls/me.wav" },
            { id: 4, name: "mi", sound: "/assets/sylls/mi.wav" },
            { id: 5, name: "ma", sound: "/assets/sylls/ma.wav" },
            { id: 6, name: "ne", sound: "/assets/sylls/ne.wav" }
        ]
    },
    {
        id: 211,
        name: "MIXPRE-173-20200415-EK-list-orig_79",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_79.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 1, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 2, name: "dja", sound: "/assets/sylls/dja.wav" },
            { id: 3, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 4, name: "ya", sound: "/assets/sylls/ya.wav" },
            { id: 5, name: "yi", sound: "/assets/sylls/yi.wav" }
        ]
    },
    {
        id: 212,
        name: "MIXPRE-173-20200415-EK-list-orig_8",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_8.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 1, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 2, name: "na", sound: "/assets/sylls/na.wav" },
            { id: 3, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 4, name: "ke", sound: "/assets/sylls/ke.wav" },
            { id: 5, name: "ba", sound: "/assets/sylls/ba.wav" },
            { id: 6, name: "bi", sound: "/assets/sylls/bi.wav" },
            { id: 7, name: "wa", sound: "/assets/sylls/wa.wav" },
            { id: 8, name: "kan", sound: "/assets/sylls/kan.wav" }
        ]
    },
    {
        id: 213,
        name: "MIXPRE-173-20200415-EK-list-orig_80",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_80.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 1, name: "wa", sound: "/assets/sylls/wa.wav" },
            { id: 2, name: "kan", sound: "/assets/sylls/kan.wav" },
            { id: 3, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 4, name: "yi", sound: "/assets/sylls/yi.wav" },
            { id: 5, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 6, name: "dja", sound: "/assets/sylls/dja.wav" },
            { id: 7, name: "ya", sound: "/assets/sylls/ya.wav" }
        ]
    },
    {
        id: 214,
        name: "MIXPRE-173-20200415-EK-list-orig_81",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_81.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 1, name: "ke", sound: "/assets/sylls/ke.wav" },
            { id: 2, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 3, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 4, name: "rre", sound: "/assets/sylls/rre.wav" },
            { id: 5, name: "di", sound: "/assets/sylls/di.wav" }
        ]
    },
    {
        id: 215,
        name: "MIXPRE-173-20200415-EK-list-orig_82",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_82.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 1, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 2, name: "rre", sound: "/assets/sylls/rre.wav" },
            { id: 3, name: "ngu", sound: "/assets/sylls/ngu.wav" },
            { id: 4, name: "me", sound: "/assets/sylls/me.wav" }
        ]
    },
    {
        id: 216,
        name: "MIXPRE-173-20200415-EK-list-orig_83",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_83.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "bu", sound: "/assets/sylls/bu.wav" },
            { id: 1, name: "bi", sound: "/assets/sylls/bi.wav" },
            { id: 2, name: "be", sound: "/assets/sylls/be.wav" },
            { id: 3, name: "ku", sound: "/assets/sylls/ku.wav" },
            { id: 4, name: "bun", sound: "/assets/sylls/bun.wav" },
            { id: 5, name: "ben", sound: "/assets/sylls/ben.wav" },
            { id: 6, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 7, name: "nga", sound: "/assets/sylls/nga.wav" }
        ]
    },
    {
        id: 217,
        name: "MIXPRE-173-20200415-EK-list-orig_84",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_84.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ku", sound: "/assets/sylls/ku.wav" },
            { id: 1, name: "bu", sound: "/assets/sylls/bu.wav" },
            { id: 2, name: "bi", sound: "/assets/sylls/bi.wav" },
            { id: 3, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 4, name: "ba", sound: "/assets/sylls/ba.wav" },
            { id: 5, name: "ka", sound: "/assets/sylls/ka.wav" }
        ]
    },
    {
        id: 218,
        name: "MIXPRE-173-20200415-EK-list-orig_85",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_85.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ku", sound: "/assets/sylls/ku.wav" },
            { id: 1, name: "ke", sound: "/assets/sylls/ke.wav" },
            { id: 2, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 3, name: "bu", sound: "/assets/sylls/bu.wav" },
            { id: 4, name: "kun", sound: "/assets/sylls/kun.wav" },
            { id: 5, name: "kan", sound: "/assets/sylls/kan.wav" },
            { id: 6, name: "bun", sound: "/assets/sylls/bun.wav" },
            { id: 7, name: "ben", sound: "/assets/sylls/ben.wav" },
            { id: 8, name: "kuk", sound: "/assets/sylls/kuk.wav" },
            { id: 9, name: "be", sound: "/assets/sylls/be.wav" },
            { id: 10, name: "bi", sound: "/assets/sylls/bi.wav" },
            { id: 11, name: "ba", sound: "/assets/sylls/ba.wav" },
            { id: 12, name: "djal", sound: "/assets/sylls/djal.wav" },
            { id: 13, name: "ni", sound: "/assets/sylls/ni.wav" }
        ]
    },
    {
        id: 219,
        name: "MIXPRE-173-20200415-EK-list-orig_86",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_86.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ku", sound: "/assets/sylls/ku.wav" },
            { id: 1, name: "ke", sound: "/assets/sylls/ke.wav" },
            { id: 2, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 3, name: "bu", sound: "/assets/sylls/bu.wav" },
            { id: 4, name: "be", sound: "/assets/sylls/be.wav" },
            { id: 5, name: "bi", sound: "/assets/sylls/bi.wav" },
            { id: 6, name: "kun", sound: "/assets/sylls/kun.wav" },
            { id: 7, name: "kan", sound: "/assets/sylls/kan.wav" },
            { id: 8, name: "bun", sound: "/assets/sylls/bun.wav" },
            { id: 9, name: "ben", sound: "/assets/sylls/ben.wav" },
            { id: 10, name: "ne", sound: "/assets/sylls/ne.wav" },
            { id: 11, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 12, name: "rne", sound: "/assets/sylls/rne.wav" },
            { id: 13, name: "me", sound: "/assets/sylls/me.wav" },
            { id: 14, name: "ba", sound: "/assets/sylls/ba.wav" },
            { id: 15, name: "men", sound: "/assets/sylls/men.wav" }
        ]
    },
    {
        id: 220,
        name: "MIXPRE-173-20200415-EK-list-orig_87",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_87.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "bu", sound: "/assets/sylls/bu.wav" },
            { id: 1, name: "bi", sound: "/assets/sylls/bi.wav" },
            { id: 2, name: "be", sound: "/assets/sylls/be.wav" },
            { id: 3, name: "ku", sound: "/assets/sylls/ku.wav" },
            { id: 4, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 5, name: "bun", sound: "/assets/sylls/bun.wav" },
            { id: 6, name: "ben", sound: "/assets/sylls/ben.wav" },
            { id: 7, name: "di", sound: "/assets/sylls/di.wav" }
        ]
    },
    {
        id: 221,
        name: "MIXPRE-173-20200415-EK-list-orig_88",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_88.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ku", sound: "/assets/sylls/ku.wav" },
            { id: 1, name: "ke", sound: "/assets/sylls/ke.wav" },
            { id: 2, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 3, name: "bu", sound: "/assets/sylls/bu.wav" },
            { id: 4, name: "kun", sound: "/assets/sylls/kun.wav" },
            { id: 5, name: "kan", sound: "/assets/sylls/kan.wav" },
            { id: 6, name: "bun", sound: "/assets/sylls/bun.wav" },
            { id: 7, name: "ben", sound: "/assets/sylls/ben.wav" },
            { id: 8, name: "di", sound: "/assets/sylls/di.wav" }
        ]
    },
    {
        id: 222,
        name: "MIXPRE-173-20200415-EK-list-orig_89",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_89.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 1, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 2, name: "bi", sound: "/assets/sylls/bi.wav" },
            { id: 3, name: "ke", sound: "/assets/sylls/ke.wav" },
            { id: 4, name: "be", sound: "/assets/sylls/be.wav" },
            { id: 5, name: "ben", sound: "/assets/sylls/ben.wav" }
        ]
    },
    {
        id: 223,
        name: "MIXPRE-173-20200415-EK-list-orig_9",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_9.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 1, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 2, name: "di", sound: "/assets/sylls/di.wav" }
        ]
    },
    {
        id: 224,
        name: "MIXPRE-173-20200415-EK-list-orig_90",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_90.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ke", sound: "/assets/sylls/ke.wav" },
            { id: 1, name: "ka", sound: "/assets/sylls/ka.wav" }
        ]
    },
    {
        id: 225,
        name: "MIXPRE-173-20200415-EK-list-orig_91",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_91.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ku", sound: "/assets/sylls/ku.wav" },
            { id: 1, name: "ke", sound: "/assets/sylls/ke.wav" },
            { id: 2, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 3, name: "bu", sound: "/assets/sylls/bu.wav" },
            { id: 4, name: "kun", sound: "/assets/sylls/kun.wav" },
            { id: 5, name: "kan", sound: "/assets/sylls/kan.wav" },
            { id: 6, name: "bun", sound: "/assets/sylls/bun.wav" },
            { id: 7, name: "ben", sound: "/assets/sylls/ben.wav" },
            { id: 8, name: "be", sound: "/assets/sylls/be.wav" },
            { id: 9, name: "ba", sound: "/assets/sylls/ba.wav" },
            { id: 10, name: "bi", sound: "/assets/sylls/bi.wav" },
            { id: 11, name: "me", sound: "/assets/sylls/me.wav" }
        ]
    },
    {
        id: 226,
        name: "MIXPRE-173-20200415-EK-list-orig_92",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_92.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ku", sound: "/assets/sylls/ku.wav" },
            { id: 1, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 2, name: "bu", sound: "/assets/sylls/bu.wav" },
            { id: 3, name: "be", sound: "/assets/sylls/be.wav" },
            { id: 4, name: "ba", sound: "/assets/sylls/ba.wav" }
        ]
    },
    {
        id: 227,
        name: "MIXPRE-173-20200415-EK-list-orig_93",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_93.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 1, name: "rre", sound: "/assets/sylls/rre.wav" },
            { id: 2, name: "yi", sound: "/assets/sylls/yi.wav" },
            { id: 3, name: "we", sound: "/assets/sylls/we.wav" },
            { id: 4, name: "wa", sound: "/assets/sylls/wa.wav" },
            { id: 5, name: "me", sound: "/assets/sylls/me.wav" },
            { id: 6, name: "ma", sound: "/assets/sylls/ma.wav" }
        ]
    },
    {
        id: 228,
        name: "MIXPRE-173-20200415-EK-list-orig_94",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_94.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "yi", sound: "/assets/sylls/yi.wav" },
            { id: 1, name: "ke", sound: "/assets/sylls/ke.wav" },
            { id: 2, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 3, name: "ne", sound: "/assets/sylls/ne.wav" },
            { id: 4, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 5, name: "nga", sound: "/assets/sylls/nga.wav" }
        ]
    },
    {
        id: 229,
        name: "MIXPRE-173-20200415-EK-list-orig_95",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_95.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ku", sound: "/assets/sylls/ku.wav" },
            { id: 1, name: "bu", sound: "/assets/sylls/bu.wav" },
            { id: 2, name: "kun", sound: "/assets/sylls/kun.wav" },
            { id: 3, name: "bun", sound: "/assets/sylls/bun.wav" },
            { id: 4, name: "ben", sound: "/assets/sylls/ben.wav" },
            { id: 5, name: "ne", sound: "/assets/sylls/ne.wav" },
            { id: 6, name: "rne", sound: "/assets/sylls/rne.wav" },
            { id: 7, name: "be", sound: "/assets/sylls/be.wav" },
            { id: 8, name: "ke", sound: "/assets/sylls/ke.wav" }
        ]
    },
    {
        id: 230,
        name: "MIXPRE-173-20200415-EK-list-orig_96",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_96.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ku", sound: "/assets/sylls/ku.wav" },
            { id: 1, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 2, name: "bu", sound: "/assets/sylls/bu.wav" },
            { id: 3, name: "bi", sound: "/assets/sylls/bi.wav" },
            { id: 4, name: "kan", sound: "/assets/sylls/kan.wav" },
            { id: 5, name: "bun", sound: "/assets/sylls/bun.wav" },
            { id: 6, name: "ben", sound: "/assets/sylls/ben.wav" },
            { id: 7, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 8, name: "rne", sound: "/assets/sylls/rne.wav" },
            { id: 9, name: "be", sound: "/assets/sylls/be.wav" },
            { id: 10, name: "ba", sound: "/assets/sylls/ba.wav" },
            { id: 11, name: "ke", sound: "/assets/sylls/ke.wav" }
        ]
    },
    {
        id: 231,
        name: "MIXPRE-173-20200415-EK-list-orig_97",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_97.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 1, name: "rre", sound: "/assets/sylls/rre.wav" },
            { id: 2, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 3, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 4, name: "wo", sound: "/assets/sylls/wo.wav" },
            { id: 5, name: "wa", sound: "/assets/sylls/wa.wav" },
            { id: 6, name: "ma", sound: "/assets/sylls/ma.wav" },
            { id: 7, name: "me", sound: "/assets/sylls/me.wav" },
            { id: 8, name: "rne", sound: "/assets/sylls/rne.wav" },
            { id: 9, name: "ke", sound: "/assets/sylls/ke.wav" },
            { id: 10, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 11, name: "ku", sound: "/assets/sylls/ku.wav" }
        ]
    },
    {
        id: 232,
        name: "MIXPRE-173-20200415-EK-list-orig_98",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_98.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 1, name: "ne", sound: "/assets/sylls/ne.wav" },
            { id: 2, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 3, name: "bu", sound: "/assets/sylls/bu.wav" },
            { id: 4, name: "ku", sound: "/assets/sylls/ku.wav" },
            { id: 5, name: "kuk", sound: "/assets/sylls/kuk.wav" },
            { id: 6, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 7, name: "ke", sound: "/assets/sylls/ke.wav" },
            { id: 8, name: "ba", sound: "/assets/sylls/ba.wav" }
        ]
    },
    {
        id: 233,
        name: "MIXPRE-173-20200415-EK-list-orig_99",
        sound: "/assets/words/MIXPRE-173-20200415-EK-list-orig_99.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 1, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 2, name: "na", sound: "/assets/sylls/na.wav" },
            { id: 3, name: "ngan", sound: "/assets/sylls/ngan.wav" },
            { id: 4, name: "kan", sound: "/assets/sylls/kan.wav" },
            { id: 5, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 6, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 7, name: "ku", sound: "/assets/sylls/ku.wav" },
            { id: 8, name: "ke", sound: "/assets/sylls/ke.wav" },
            { id: 9, name: "bu", sound: "/assets/sylls/bu.wav" },
            { id: 10, name: "bi", sound: "/assets/sylls/bi.wav" },
            { id: 11, name: "kuk", sound: "/assets/sylls/kuk.wav" },
            { id: 12, name: "ba", sound: "/assets/sylls/ba.wav" }
        ]
    },
    {
        id: 234,
        name: "kakkali",
        sound: "/assets/words/kakkali.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 1, name: "djal", sound: "/assets/sylls/djal.wav" },
            { id: 2, name: "kka", sound: "/assets/sylls/kka.wav" },
            { id: 3, name: "kan", sound: "/assets/sylls/kan.wav" },
            { id: 4, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 5, name: "yi", sound: "/assets/sylls/yi.wav" }
        ]
    },
    {
        id: 235,
        name: "morlah",
        sound: "/assets/words/morlah.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 1, name: "ma", sound: "/assets/sylls/ma.wav" },
            { id: 2, name: "me", sound: "/assets/sylls/me.wav" },
            { id: 3, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 4, name: "wa", sound: "/assets/sylls/wa.wav" },
            { id: 5, name: "na", sound: "/assets/sylls/na.wav" }
        ]
    },
    {
        id: 236,
        name: "morlahwarre",
        sound: "/assets/words/morlahwarre.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "wo", sound: "/assets/sylls/wo.wav" },
            { id: 1, name: "wa", sound: "/assets/sylls/wa.wav" },
            { id: 2, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 3, name: "ma", sound: "/assets/sylls/ma.wav" },
            { id: 4, name: "ya", sound: "/assets/sylls/ya.wav" },
            { id: 5, name: "bu", sound: "/assets/sylls/bu.wav" }
        ]
    },
    {
        id: 237,
        name: "nabadjan",
        sound: "/assets/words/nabadjan.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "na", sound: "/assets/sylls/na.wav" },
            { id: 1, name: "ne", sound: "/assets/sylls/ne.wav" },
            { id: 2, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 3, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 4, name: "bi", sound: "/assets/sylls/bi.wav" }
        ]
    },
    {
        id: 238,
        name: "nakewud",
        sound: "/assets/words/nakewud.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "na", sound: "/assets/sylls/na.wav" },
            { id: 1, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 2, name: "ke", sound: "/assets/sylls/ke.wav" },
            { id: 3, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 4, name: "yi", sound: "/assets/sylls/yi.wav" },
            { id: 5, name: "wo", sound: "/assets/sylls/wo.wav" }
        ]
    },
    {
        id: 239,
        name: "nalyawngu",
        sound: "/assets/words/nalyawngu.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "na", sound: "/assets/sylls/na.wav" },
            { id: 1, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 2, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 3, name: "ya", sound: "/assets/sylls/ya.wav" }
        ]
    },
    {
        id: 240,
        name: "namurlebe",
        sound: "/assets/words/namurlebe.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "na", sound: "/assets/sylls/na.wav" },
            { id: 1, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 2, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 3, name: "mu", sound: "/assets/sylls/mu.wav" },
            { id: 4, name: "ngu", sound: "/assets/sylls/ngu.wav" },
            { id: 5, name: "le", sound: "/assets/sylls/le.wav" },
            { id: 6, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 7, name: "we", sound: "/assets/sylls/we.wav" },
            { id: 8, name: "wa", sound: "/assets/sylls/wa.wav" },
            { id: 9, name: "be", sound: "/assets/sylls/be.wav" },
            { id: 10, name: "bi", sound: "/assets/sylls/bi.wav" },
            { id: 11, name: "me", sound: "/assets/sylls/me.wav" }
        ]
    },
    {
        id: 241,
        name: "ngadjadj",
        sound: "/assets/words/ngadjadj.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ba", sound: "/assets/sylls/ba.wav" },
            { id: 1, name: "bu", sound: "/assets/sylls/bu.wav" },
            { id: 2, name: "be", sound: "/assets/sylls/be.wav" },
            { id: 3, name: "na", sound: "/assets/sylls/na.wav" },
            { id: 4, name: "ma", sound: "/assets/sylls/ma.wav" },
            { id: 5, name: "wa", sound: "/assets/sylls/wa.wav" },
            { id: 6, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 7, name: "ke", sound: "/assets/sylls/ke.wav" }
        ]
    },
    {
        id: 242,
        name: "ngalakankinj",
        sound: "/assets/words/ngalakankinj.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ba", sound: "/assets/sylls/ba.wav" },
            { id: 1, name: "ma", sound: "/assets/sylls/ma.wav" },
            { id: 2, name: "wa", sound: "/assets/sylls/wa.wav" },
            { id: 3, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 4, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 5, name: "kan", sound: "/assets/sylls/kan.wav" },
            { id: 6, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 7, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 8, name: "me", sound: "/assets/sylls/me.wav" }
        ]
    },
    {
        id: 243,
        name: "ngalkankinj",
        sound: "/assets/words/ngalkankinj.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 1, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 2, name: "kan", sound: "/assets/sylls/kan.wav" },
            { id: 3, name: "di", sound: "/assets/sylls/di.wav" }
        ]
    },
    {
        id: 244,
        name: "ngalmadjenje",
        sound: "/assets/words/ngalmadjenje.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ma", sound: "/assets/sylls/ma.wav" },
            { id: 1, name: "na", sound: "/assets/sylls/na.wav" },
            { id: 2, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 3, name: "wa", sound: "/assets/sylls/wa.wav" },
            { id: 4, name: "mak", sound: "/assets/sylls/mak.wav" },
            { id: 5, name: "ke", sound: "/assets/sylls/ke.wav" },
            { id: 6, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 7, name: "ku", sound: "/assets/sylls/ku.wav" },
            { id: 8, name: "be", sound: "/assets/sylls/be.wav" },
            { id: 9, name: "kan", sound: "/assets/sylls/kan.wav" },
            { id: 10, name: "ben", sound: "/assets/sylls/ben.wav" },
            { id: 11, name: "bi", sound: "/assets/sylls/bi.wav" }
        ]
    },
    {
        id: 245,
        name: "ngalmadjewud",
        sound: "/assets/words/ngalmadjewud.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 1, name: "ke", sound: "/assets/sylls/ke.wav" },
            { id: 2, name: "kan", sound: "/assets/sylls/kan.wav" },
            { id: 3, name: "ma", sound: "/assets/sylls/ma.wav" },
            { id: 4, name: "me", sound: "/assets/sylls/me.wav" },
            { id: 5, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 6, name: "na", sound: "/assets/sylls/na.wav" },
            { id: 7, name: "ku", sound: "/assets/sylls/ku.wav" },
            { id: 8, name: "wo", sound: "/assets/sylls/wo.wav" },
            { id: 9, name: "wa", sound: "/assets/sylls/wa.wav" },
            { id: 10, name: "bu", sound: "/assets/sylls/bu.wav" },
            { id: 11, name: "djal", sound: "/assets/sylls/djal.wav" }
        ]
    },
    {
        id: 246,
        name: "ngalmurlebe",
        sound: "/assets/words/ngalmurlebe.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 1, name: "mu", sound: "/assets/sylls/mu.wav" },
            { id: 2, name: "ma", sound: "/assets/sylls/ma.wav" },
            { id: 3, name: "bu", sound: "/assets/sylls/bu.wav" },
            { id: 4, name: "ngu", sound: "/assets/sylls/ngu.wav" },
            { id: 5, name: "le", sound: "/assets/sylls/le.wav" },
            { id: 6, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 7, name: "rne", sound: "/assets/sylls/rne.wav" },
            { id: 8, name: "bi", sound: "/assets/sylls/bi.wav" },
            { id: 9, name: "be", sound: "/assets/sylls/be.wav" },
            { id: 10, name: "di", sound: "/assets/sylls/di.wav" }
        ]
    },
    {
        id: 247,
        name: "ngalmurlebe_2",
        sound: "/assets/words/ngalmurlebe_2.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ma", sound: "/assets/sylls/ma.wav" },
            { id: 1, name: "ba", sound: "/assets/sylls/ba.wav" },
            { id: 2, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 3, name: "na", sound: "/assets/sylls/na.wav" },
            { id: 4, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 5, name: "mu", sound: "/assets/sylls/mu.wav" },
            { id: 6, name: "ngu", sound: "/assets/sylls/ngu.wav" },
            { id: 7, name: "ngun", sound: "/assets/sylls/ngun.wav" },
            { id: 8, name: "le", sound: "/assets/sylls/le.wav" },
            { id: 9, name: "ne", sound: "/assets/sylls/ne.wav" },
            { id: 10, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 11, name: "me", sound: "/assets/sylls/me.wav" },
            { id: 12, name: "be", sound: "/assets/sylls/be.wav" },
            { id: 13, name: "bi", sound: "/assets/sylls/bi.wav" },
            { id: 14, name: "rre", sound: "/assets/sylls/rre.wav" },
            { id: 15, name: "rri", sound: "/assets/sylls/rri.wav" }
        ]
    },
    {
        id: 248,
        name: "ngalngalayhngu",
        sound: "/assets/words/ngalngalayhngu.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "wa", sound: "/assets/sylls/wa.wav" },
            { id: 1, name: "na", sound: "/assets/sylls/na.wav" },
            { id: 2, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 3, name: "ni", sound: "/assets/sylls/ni.wav" }
        ]
    },
    {
        id: 249,
        name: "ngalngalayhngu_2",
        sound: "/assets/words/ngalngalayhngu_2.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 1, name: "ngan", sound: "/assets/sylls/ngan.wav" },
            { id: 2, name: "na", sound: "/assets/sylls/na.wav" },
            { id: 3, name: "ngu", sound: "/assets/sylls/ngu.wav" },
            { id: 4, name: "ku", sound: "/assets/sylls/ku.wav" }
        ]
    },
    {
        id: 250,
        name: "ngalyawngu",
        sound: "/assets/words/ngalyawngu.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 1, name: "ya", sound: "/assets/sylls/ya.wav" },
            { id: 2, name: "djal", sound: "/assets/sylls/djal.wav" }
        ]
    },
    {
        id: 251,
        name: "ngalyawngu_3",
        sound: "/assets/words/ngalyawngu_3.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 1, name: "ya", sound: "/assets/sylls/ya.wav" },
            { id: 2, name: "yi", sound: "/assets/sylls/yi.wav" }
        ]
    },
    {
        id: 252,
        name: "ngalyawngu_4",
        sound: "/assets/words/ngalyawngu_4.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 1, name: "na", sound: "/assets/sylls/na.wav" },
            { id: 2, name: "wa", sound: "/assets/sylls/wa.wav" },
            { id: 3, name: "ma", sound: "/assets/sylls/ma.wav" },
            { id: 4, name: "ya", sound: "/assets/sylls/ya.wav" },
            { id: 5, name: "ngu", sound: "/assets/sylls/ngu.wav" }
        ]
    },
    {
        id: 253,
        name: "nganebadjan",
        sound: "/assets/words/nganebadjan.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ma", sound: "/assets/sylls/ma.wav" },
            { id: 1, name: "ba", sound: "/assets/sylls/ba.wav" },
            { id: 2, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 3, name: "na", sound: "/assets/sylls/na.wav" },
            { id: 4, name: "man", sound: "/assets/sylls/man.wav" },
            { id: 5, name: "marn", sound: "/assets/sylls/marn.wav" },
            { id: 6, name: "kan", sound: "/assets/sylls/kan.wav" },
            { id: 7, name: "ne", sound: "/assets/sylls/ne.wav" },
            { id: 8, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 9, name: "rne", sound: "/assets/sylls/rne.wav" },
            { id: 10, name: "wa", sound: "/assets/sylls/wa.wav" },
            { id: 11, name: "bu", sound: "/assets/sylls/bu.wav" },
            { id: 12, name: "bi", sound: "/assets/sylls/bi.wav" },
            { id: 13, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 14, name: "dja", sound: "/assets/sylls/dja.wav" },
            { id: 15, name: "ya", sound: "/assets/sylls/ya.wav" },
            { id: 16, name: "djal", sound: "/assets/sylls/djal.wav" }
        ]
    },
    {
        id: 254,
        name: "output_03",
        sound: "/assets/words/output_03.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 1, name: "ke", sound: "/assets/sylls/ke.wav" },
            { id: 2, name: "kan", sound: "/assets/sylls/kan.wav" },
            { id: 3, name: "djal", sound: "/assets/sylls/djal.wav" }
        ]
    },
    {
        id: 255,
        name: "output_04",
        sound: "/assets/words/output_04.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 1, name: "na", sound: "/assets/sylls/na.wav" },
            { id: 2, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 3, name: "ba", sound: "/assets/sylls/ba.wav" },
            { id: 4, name: "yi", sound: "/assets/sylls/yi.wav" },
            { id: 5, name: "di", sound: "/assets/sylls/di.wav" },
            { id: 6, name: "rri", sound: "/assets/sylls/rri.wav" },
            { id: 7, name: "ma", sound: "/assets/sylls/ma.wav" },
            { id: 8, name: "me", sound: "/assets/sylls/me.wav" },
            { id: 9, name: "wa", sound: "/assets/sylls/wa.wav" }
        ]
    },
    {
        id: 256,
        name: "output_05",
        sound: "/assets/words/output_05.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "na", sound: "/assets/sylls/na.wav" },
            { id: 1, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 2, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 3, name: "ma", sound: "/assets/sylls/ma.wav" },
            { id: 4, name: "wa", sound: "/assets/sylls/wa.wav" },
            { id: 5, name: "bu", sound: "/assets/sylls/bu.wav" }
        ]
    },
    {
        id: 257,
        name: "output_06",
        sound: "/assets/words/output_06.wav",
        done: false,
        store: [],
        sylls: [

            { id: 0, name: "ka", sound: "/assets/sylls/ka.wav" },
            { id: 1, name: "na", sound: "/assets/sylls/na.wav" },
            { id: 2, name: "nga", sound: "/assets/sylls/nga.wav" },
            { id: 3, name: "ke", sound: "/assets/sylls/ke.wav" },
            { id: 4, name: "ni", sound: "/assets/sylls/ni.wav" },
            { id: 5, name: "ya", sound: "/assets/sylls/ya.wav" },
            { id: 6, name: "yi", sound: "/assets/sylls/yi.wav" },
            { id: 7, name: "rne", sound: "/assets/sylls/rne.wav" }
        ]
    }

];
