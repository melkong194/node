// http://localhost:3000/read?fileName=abc

var express = require('express');
var cors = require('cors');
var fs = require('fs');
var multer = require('multer');

var app = express();

app.use(cors());
app.use(express.json({limit: '50mb'}));
app.use(express.static('public'));


var upload = multer({ dest: __dirname });
var type = upload.single('file');

// INPUT DATA HERE
// uncomment to write data from wordsInput.js to words.js then comment 
// var input = require('./wordsInput');
// fs.writeFileSync('words.js', JSON.stringify(input), 'utf-8');


app.get('/read', readPy);
app.post('/save', dataWrite);
app.get('/getData', dataRead);
app.post('/sendFile', type, fileRead);


function fileRead(req, res) {
    var buf = new Buffer.from(req.body.blob, 'base64'); // decode
    fs.writeFile("test.wav", buf, function (err) {
        if (err) {
            res.send("0");
        } else {
            console.log("success!");
            res.send("1");
        }
    })

}

function readPy(req, res) {
    var spawn = require("child_process").spawn;
    var process = spawn('python3', ["./transcribe.py",
        req.query.file + ".wav"]);
    process.stdout.on('data', function (data) {
        console.log(data.toString());
        res.send(data.toString());
    })
}

function dataRead(req, res) {
    var data = fs.readFileSync('words.js', 'utf-8');
    res.send(data);
}

function dataWrite(req, res) {
    var data = JSON.parse(fs.readFileSync('words.js', 'utf-8'));
    let e = JSON.parse(req.body.word);
    let i = null;
    for(i = 0 ; i<data.length; i++){
        if(data[i].id == e.id){
            p = i;
            break;
        }
    }
    data[i] = e;
    fs.writeFileSync('words.js', JSON.stringify(data), 'utf-8');

    res.send(e);

}


const port = process.env.PORT || 3000;
app.listen(port, () => console.log(`Server run on port: ${port}`));
